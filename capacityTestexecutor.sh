#!/bin/sh

JMETER_PATH=apache-jmeter-2.13/bin
JMETER_LIB=apache-jmeter-2.13/lib

THREADS=$1
TEST_NAME=$2
BUILD_NUMBER=$3
TEST_DURATION=$4
JMX_NAME=$5

BASE_PATH=/opt/${TEST_NAME}/${BUILD_NUMBER}
NAME_OF_TEST=${TEST_NAME}"_"${BUILD_NUMBER}

#psr executers
#SLACK_URL=https://hooks.slack.com/services/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

#psr-discussion
#SLACK_URL=https://hooks.slack.com/services/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

#psr
SLACK_URL=https://hooks.slack.com/services/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

sendMessageToSlack()
{
#renaming headers from aggregate report file
sed -i 's/aggregate_report_//g' $1
sed -i 's/count/No. of Requests/g' $1
sed -i 's/average/Avg Response Time(ms)/g' $1
sed -i 's/error%/Error%/g' $1
sed -i 's/rate/Throughput/g' $1

#cut - extract portion of text from a file by selecting columns. 
#column - Display File Contents In Column Format.
FILE_CONTENT=`cut -d, -f4-7,9-11 --complement $1 |  column -s, -t`

SLACK_MESSAGE="\`\`\`$FILE_CONTENT\`\`\`"

MSG="{ \"text\": \"*${JMX_NAME}*\", \"attachments\": [ { \"color\": \"good\", \"fields\": [ { \"title\": \"Start Time\", \"value\": \"${startTime}\", \"short\": true }, { \"title\": \"End Time\", \"value\": \"${endTime}\", \"short\": true } ] }, { \"color\": \"#000000\", \"text\": \"Aggregate Report\", \"mrkdwn_in\": [ \"text\" ] }, { \"color\": \"#000000\", \"text\": \"${SLACK_MESSAGE}\", \"mrkdwn_in\": [ \"text\" ] } ] }"

echo "sending message to slack"
echo ""
echo ""
curl -X POST --data "payload=${MSG}" ${SLACK_URL}

echo ""
echo "********** Aggregate Report File ************* "
echo ""
cat $1 | column -s, -t
echo "********************************************** "
echo ""

}

echo ""
echo "********************************************** "
echo "Number of concurrent threads : ${THREADS} "
echo "Test Name : ${TEST_NAME} "
echo "********************************************** "
echo ""
echo "********************************************** "
echo "Base path for Test : ${BASE_PATH} "
echo "Name of Test jtl file : ${NAME_OF_TEST} "
echo "********************************************** "
echo ""
echo "********************************************** "
echo "Duration for test to run(in seconds). : ${TEST_DURATION} "
echo "Name of Jmx file : ${JMX_NAME} "
echo "********************************************** "

#creating base directory.
mkdir -p ${BASE_PATH}

#Delete Files from result folder whhich are Older Than x Days.
find /opt/${TEST_NAME}/ -mtime +5 -exec rm -r {} \;

#copy datasourse(input csv) to base path.
cp -r datasource/* ${BASE_PATH}

startTime=$(date -d '+5 hour 30 min' -u +"%Y-%m-%dT%H:%M:%S.%3NZ")
echo "Start Time : ${startTime}" >> ${BASE_PATH}/testduration.txt

echo ""
echo "********************************************** "
echo "Start Time : ${startTime}"
echo "********************************************** "
echo ""

echo "${JMETER_PATH}/jmeter.sh -n -t Capacity-Tests/${TEST_NAME}/${JMX_NAME}.jmx -l  ${BASE_PATH}/${NAME_OF_TEST}.jtl -Joutput_folder=${BASE_PATH}/ -Jerrorfile=${BASE_PATH}/ -Jlog -Jconcurrentthreads=${THREADS} -Jduration=${TEST_DURATION} > ${BASE_PATH}/jmeterconsoleoutput.txt"

cp -r csv-to-html-table-master/* ${BASE_PATH}/
sh ${JMETER_PATH}/jmeter.sh -n -t Capacity-Tests/${TEST_NAME}/${JMX_NAME}.jmx -l  ${BASE_PATH}/${NAME_OF_TEST}.jtl -Joutput_folder=${BASE_PATH}/ -Jerrorfile=${BASE_PATH}/ -Jlog -Jconcurrentthreads=${THREADS} -Jduration=${TEST_DURATION} > ${BASE_PATH}/jmeterconsoleoutput.txt

java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-csv "${BASE_PATH}/data/agreegatereport.csv" --input-jtl "${BASE_PATH}/${NAME_OF_TEST}.jtl" --plugin-type AggregateReport

java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-png "${BASE_PATH}/images/RTOT.png" --input-jtl "${BASE_PATH}/${NAME_OF_TEST}.jtl" --plugin-type ResponseTimesOverTime --width 800 --height 600

java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-png "${BASE_PATH}/images/TPS.png" --input-jtl "${BASE_PATH}/${NAME_OF_TEST}.jtl" --plugin-type TransactionsPerSecond --width 800 --height 600

java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-png "${BASE_PATH}/images/HPS.png" --input-jtl "${BASE_PATH}/${NAME_OF_TEST}.jtl" --plugin-type HitsPerSecond --width 800 --height 600

mv  jmeter.log  ${BASE_PATH}

echo ""
echo ""
endTime=$(date -d '+5 hour 30 min' -u +"%Y-%m-%dT%H:%M:%S.%3NZ")

echo "********************************************** "
echo "End Time : ${endTime}"
echo "********************************************** "

echo "End Time : ${endTime}" >> ${BASE_PATH}/testduration.txt

#send message to slack.
sendMessageToSlack ${BASE_PATH}/data/agreegatereport.csv