#!/bin/sh

NEWRELIC=$1
GRAPHANA=$2
KIBANA=$3
ZIPKIN=$4
DIRECTORY=$5

#psr executers
#SLACK_URL=https://hooks.slack.com/services/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

#psr-discussion
#SLACK_URL=https://hooks.slack.com/services/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
#psr
SLACK_URL=https://hooks.slack.com/services/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

sendMessageToSlack()
{
SLACK_MESSAGE="*Newrelic* : $NEWRELIC"$'\n'"*Grafana* : $GRAPHANA"$'\n'"*Kibana* : $KIBANA"$'\n'"*Zipkin* : $ZIPKIN"$'\n'"*Result-Details* : $DIRECTORY"

MSG="{ \"text\": \"*\`Links : \`*\", \"attachments\": [{ \"color\": \"good\", \"text\": \"${SLACK_MESSAGE}\", \"mrkdwn_in\": [ \"text\" ] }]} "

curl -X POST --data "payload=${MSG}" ${SLACK_URL}
}

echo ""
echo "********************************************** "
echo "Newrelic : $NEWRELIC"
echo "Grafana : $GRAPHANA"
echo "Kibana : $KIBANA"
echo "Zipkin : $ZIPKIN"
echo "Result-Details : $DIRECTORY"
echo "********************************************** "

#send message to slack.
sendMessageToSlack  
