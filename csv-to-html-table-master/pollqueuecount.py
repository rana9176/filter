import pika
import sys

print "* Connecting to RabbitMQ broker"

print sys.argv[1]
print sys.argv[2]
print sys.argv[3]
print sys.argv[4]
print sys.argv[5]

try:
    credentials = pika.PlainCredentials(sys.argv[4], sys.argv[5])
    parameters = pika.ConnectionParameters (host=sys.argv[1], port=int(sys.argv[2]), virtual_host=sys.argv[3],credentials=credentials)
    print parameters
    connection = pika.BlockingConnection(parameters)
    print "Connection created"
    channel = connection.channel()
    q = channel.queue_declare(q_name)
    q_len = q.method.message_count
    print q_len
	

except KeyboardInterrupt:
    channel.stop_consuming()
    connection.close()
except Exception, e:
    print 'Exception occured'
    print e
    #connection.close()
    sys.exit(1)
connection.close()


#100.73.77.184 15672 CUS_PART1 epmp epmp