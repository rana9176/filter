import pandas as pd
from pandas.io.parsers import read_csv
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import datetime
from collections import defaultdict
import sys
import os
from scipy.stats import norm
matplotlib.use('Agg')

def average(seq):
	avg = (int(sum(seq)) / len(seq))
	return avg
	
def percentile(seq, percentile):
	i = int(len(seq) * (percentile / 100.0))
	seq.sort()
	return seq[i]

def plothistogram(avg_responsetime,newpath):
	# Fit a normal distribution to the data:
	mu, std = norm.fit(avg_responsetime)
	# Plot the histogram.
	#normed=1
	fig,ax = plt.subplots()
	#fig =  plt.figure(figsize=(10.0, 10.0))
	#counts,bins,patches = plt.hist(avg_responsetime, bins=range(min(avg_responsetime), max(avg_responsetime) + 10, 10), color='g')
        counts,bins,patches = plt.hist(avg_responsetime, bins=10, color='g')
	print counts
	print bins
	print patches
	ax.set_xticks(bins)
	#fig =  plt.figure(figsize=(10.0, 10.0))
	# Label the raw counts and the percentages below the x-axis...
	bin_centers = 0.5 * np.diff(bins) + bins[:-1]
	for count, x in zip(counts, bin_centers):
		# Label the raw counts
		#ax.annotate(str(count), xy=(x, 0), xycoords=('data', 'axes fraction'),
		#xytext=(0, -18), textcoords='offset points', va='top', ha='center')

		# Label the percentages
		percent = '%0.0f%%' % (100 * float(count) / counts.sum())
		ax.annotate(percent, xy=(x, 0), xycoords=('data', 'axes fraction'),
			xytext=(0, -32), textcoords='offset points', va='top', ha='center')
	'''
	plt.subplots_adjust(bottom=0.15)
	xmin, xmax = plt.xlim()
	x = np.linspace(xmin, xmax, 100)
	p = norm.pdf(x, mu, std)
	plt.plot(x, p, 'k', linewidth=2)
	'''
	title = "Mean = %.2f,  Standrad Deviation = %.2f" % (mu, std)
	plt.title(title)
	plt.ylabel("Frequency", fontsize=16)
	plt.xlabel("Response Time in (sec)", fontsize=10)
	responsetimegraph1 = newpath + r'/' + r'eventstestfrequencydistribution.png'
	#manager = plt.get_current_fig_manager()
	#manager.resize(*manager.window.maxsize())
	plt.savefig(responsetimegraph1)
	#plt.show()
	

def guassiandistribution(avg_responsetime):
	mu, std = norm.fit(avg_responsetime)
	xmin, xmax = plt.xlim()
	x = np.linspace(xmin, xmax, 100)
	p = norm.pdf(x, mu, std)
	plt.plot(x, p, 'k', linewidth=2)
	title = "Guassian Distribution"
	plt.title(title)
	plt.ylabel("Probablity", fontsize=16)
	plt.xlabel("Response Time in (sec)", fontsize=10)
	responsetimegraph1 = newpath + r'/' + r'eventstest95thtime.png'
	plt.savefig(responsetimegraph1)
	#plt.show()
	

def main():	
	csvfilename = sys.argv[1]
	temp =csvfilename.split('.')
	currentdir = os.getcwd()
	newpath = currentdir + '/' + temp[0]
	print newpath
	os.makedirs(newpath)

	df = read_csv(csvfilename,header=None)
	print "Column Headers", df.columns
	#print "Data Types", df.dtypes
	df[4] = pd.to_datetime(df[4],format='%Y-%m-%dT%H:%M:%S.%fZ')
	df[5] = pd.to_datetime(df[5],format='%Y-%m-%dT%H:%M:%S.%fZ')
	df[6] = df[5]-df[4]
	df[6]= df[6]/pd.np.timedelta64(1, 'us')
	df[6] = (df[6]/1000)
	df[6]=df[6].astype(int)
        df[6]=pd.to_numeric(df[6], errors='coerce')
	print "Data Types", df.dtypes	
	df[6].to_csv('diff-output.csv')
	plothistogram(df[6].tolist(),newpath)

        #Create new Dataframe for comparision
	df2 = pd.DataFrame(df[5])
	df2[1] = df[6]
	print "Data Types", df2.dtypes
	df2[3] = df2[5].values.astype('datetime64[m]') #this is for minute distribution
	#df2[3] = df2[5].values.astype('datetime64[s]') this is for seconds distribution 

	endtimeseries =	 pd.Series(df2[3],name='s1')
	endtimeseries=endtimeseries.astype(str)
	print '*************************************'
	print endtimeseries.dtypes
	responsetimeseries = pd.Series(df2[1],name='s2')
	#print endtimeseries.values
	#print responsetimeseries.values
	finaldf = pd.concat([endtimeseries, responsetimeseries], axis=1)
	#print finaldf
	time_groups = finaldf.groupby('s1')
	i = 0


	ticklist=[]
	keys=[]
	avg_responsetime=[]
	percentile_95_response_time_points= []
	percentile_99_response_time_points = []

	for name, group in time_groups:
	   print "Group", i, name
	   i = i +1
	   #print group ['s2']
	   avg = average (group ['s2'].tolist())
	   avg_responsetime.append(avg)
	   pct_95 = percentile(group ['s2'].tolist(),95)
	   percentile_95_response_time_points.append(pct_95)
	   pct_99 = percentile(group ['s2'].tolist(),99)
	   percentile_99_response_time_points.append(pct_99)
	   keys.append(name)
	   ticklist.append(i)
	#print avg_responsetime
	#print keys

	final_avg =	 average(responsetimeseries.tolist())
	final_95 = percentile(responsetimeseries.tolist(),95)
	final_99 = percentile(responsetimeseries.tolist(),99)
	print "Final Avg. Response Time:",(final_avg)
	print "Final 95th. Response Time:",(final_95)
	print "Final 99th Response Time:",(final_99)

	responsfilepath = newpath + r'/' + r'responsetimes.txt'

	infile = open (responsfilepath,'w')
	lines = "Final Avg. Response Time:"+ str(final_avg)
	infile.write(lines + '\n')
	lines = "Final 95th. Response Time:"+ str(final_95)
	infile.write(lines + '\n')
	lines = "Final 99th. Response Time:"+ str(final_99)
	infile.write(lines + '\n')
	infile.flush()
	infile.close()

	#export data into csv and json for graph

	percentile_list = pd.DataFrame({'EndTime': keys,
		 'Avg. Response Time': avg_responsetime,
		 '95th Percentile Response Time': percentile_95_response_time_points,
		 '99th Percentile Response Time': percentile_99_response_time_points
		})
	
	print "HTML Column Headers", percentile_list.columns
	#percentile_list[['EndTime','Avg. Response Time','95th Percentile Response Time','99th Percentile Response Time']]=percentile_list[['95th Percentile Response Time','99th Percentile Response Time','Avg. Response Time','EndTime']]
	
        cols = percentile_list.columns.tolist()
	cols = cols[-1:] + cols[:-1]
	percentile_list = percentile_list[cols]
	#print percentile_list
	
	percentile_list.to_csv('orignal-output.csv')
	percentile_list=percentile_list.transpose()
	percentile_list_new=percentile_list
	
	percentile_list_new.to_csv('output.csv',header=False) # this is for response time html
	#percentile_list_new.to_json('output.json')
	#plothistogram(avg_responsetime,newpath)
	#guassiandistribution(avg_responsetime)

if __name__ == '__main__':
		main()






'''
n,bins,patches = plt.hist(avg_responsetime,bins=20)

print n
print bins
print patches

plt.show()
'''
'''

plt.ylabel("Response Time in (ms)", fontsize=16)
plt.xlabel("End Time", fontsize=10)
plt.axis([0, 50, 0, 50])
#plt.gcf().subplots_adjust(bottom=0.15)
fig = plt.figure()
ax = fig.add_subplot(111)
#plt.plot(ticklist, avg_responsetime, label="average")
plt.plot(ticklist, avg_responsetime)
for xy in zip(ticklist, avg_responsetime):										 # <--
	ax.annotate('(%s, %s)' % xy, xy=xy, textcoords='data') # <--
#plt.plot(ticklist, percentile_95_response_time_points, label="95 percentile")
#for xy in zip(ticklist, percentile_95_response_time_points):										# <--
	#ax.annotate('(%s, %s)' % xy, xy=xy, textcoords='data') # <--
#plt.plot(ticklist,percentile_99_response_time_points, label="99 percentile")
#for xy in zip(ticklist, percentile_99_response_time_points):										# <--
	#ax.annotate('(%s, %s)' % xy, xy=xy, textcoords='data') # <--
plt.xticks(ticklist, keys)
plt.legend()
avgresponsetimegraph = newpath + r'/' + r'eventstestavgtime.png'
plt.savefig(avgresponsetimegraph)
#plt.show()

'''

'''

plt.ylabel("Response Time in (ms)", fontsize=16)
plt.xlabel("End Time", fontsize=10)
plt.axis([0, 50, 0, 50])
#plt.gcf().subplots_adjust(bottom=0.15)
fig = plt.figure()
ax = fig.add_subplot(111)
#plt.plot(ticklist, percentile_95_response_time_points, label="95 percentile")
plt.plot(ticklist, percentile_95_response_time_points)
for xy in zip(ticklist, percentile_95_response_time_points):									   # <--
	ax.annotate('(%s, %s)' % xy, xy=xy, textcoords='data') # <--
#plt.plot(ticklist,percentile_99_response_time_points, label="99 percentile")
#for xy in zip(ticklist, percentile_99_response_time_points):										# <--
	#ax.annotate('(%s, %s)' % xy, xy=xy, textcoords='data') # <--
plt.xticks(ticklist, keys)
plt.legend()
responsetimegraph1 = newpath + r'/' + r'eventstest95thtime.png'
plt.savefig(responsetimegraph1)
#plt.show()

plt.ylabel("Response Time in (ms)", fontsize=16)
plt.xlabel("End Time", fontsize=10)
plt.axis([0, 50, 0, 50])
#plt.gcf().subplots_adjust(bottom=0.15)
fig = plt.figure()
ax = fig.add_subplot(111)
#plt.plot(ticklist,percentile_99_response_time_points, label="99 percentile")
plt.plot(ticklist,percentile_99_response_time_points)
for xy in zip(ticklist, percentile_99_response_time_points):									   # <--
	ax.annotate('(%s, %s)' % xy, xy=xy, textcoords='data') # <--
plt.xticks(ticklist, keys)
plt.legend()
responsetimegraph2 = newpath + r'/' + r'eventstest99thtime.png'
plt.savefig(responsetimegraph2)
#plt.show()
'''
























