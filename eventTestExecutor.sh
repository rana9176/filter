#!/bin/sh

JMETER_PATH=apache-jmeter-2.13/bin
JMETER_LIB=apache-jmeter-2.13/lib

THREADS=$1
TEST_NAME=$2
BUILD_NUMBER=$3

BASE_PATH=/opt/${TEST_NAME}/${BUILD_NUMBER}
NAME_OF_TEST=${TEST_NAME}"_"${BUILD_NUMBER}

#psr executers
#SLACK_URL=https://hooks.slack.com/services/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

#psr-discussion
SLACK_URL=https://hooks.slack.com/services/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

sendMessageToSlack()
{
#remove unused headers from aggregate report file
sed -i 's/aggregate_report_//g' $1

FILE_CONTENT=`sed -i '$ d' $1 | cut -d, -f4-7,10-11 --complement $1 |  column -s, -t`
SLACK_MESSAGE="\`\`\`$FILE_CONTENT\`\`\`"

MSG="{ \"text\": \"*${TEST_NAME} Test*\", \"attachments\": [ { \"color\": \"good\", \"fields\": [ { \"title\": \"Start Time\", \"value\": \"${startTime}\", \"short\": true }, { \"title\": \"End Time\", \"value\": \"${endTime}\", \"short\": true } ] }, { \"color\": \"#000000\", \"text\": \"Aggregate Report\", \"mrkdwn_in\": [ \"text\" ] }, { \"color\": \"#000000\", \"text\": \"${SLACK_MESSAGE}\", \"mrkdwn_in\": [ \"text\" ] } ] }"

echo "sending message to slack"
echo ""
echo ""
curl -X POST --data "payload=${MSG}" ${SLACK_URL}
}

echo ""
echo "********************************************** "
echo "Number of concurrent threads : ${THREADS} "
echo "Test Name : ${TEST_NAME} "
echo "********************************************** "
echo ""
echo "********************************************** "
echo "Base path for Test : ${BASE_PATH} "
echo "Name of Test jtl file : ${NAME_OF_TEST} "
echo "********************************************** "

mkdir -p ${BASE_PATH}

#copy datasourse(input csv) to base path
cp -r datasource/* ${BASE_PATH}

startTime=$(date -d '+5 hour 30 min' -u +"%Y-%m-%dT%H:%M:%S.%3NZ")
echo "Start Time : ${startTime}" >> ${BASE_PATH}/testduration.txt

echo ""
echo "********************************************** "
echo "Start Time : ${startTime}"
echo "********************************************** "
echo ""

validationserviceip=100.122.195.121
tokenserviceip=100.122.195.121

#sh jmeter.sh -n -t ${JMX_PATH=}/Events-Capacity.jmx -l ${test_result_folder}/${Test_NAME}.jtl -Joutput_folder=${BASE_PATH}/ -Jerrorfile=${test_result_folder}/ -Jlog -Jconcurrentthreads=$i -Jduration=3600 -JvalidationService=100.73.97.71 -Jtokenserver=192.168.3.65 -Jresulttemp=${test_result_folder}/ > ${test_result_folder}/jmeterconsoleoutput.txt

echo "${JMETER_PATH}/jmeter.sh -n -t Capacity-Tests/${TEST_NAME}/PostOpstate.jmx -l  ${BASE_PATH}/${NAME_OF_TEST}.jtl -Joutput_folder=${BASE_PATH}/ -Jerrorfile=${BASE_PATH}/ -Jlog -Jconcurrentthreads=${THREADS} -Jduration=14400 > ${BASE_PATH}/jmeterconsoleoutput.txt"

sh ${JMETER_PATH}/jmeter.sh -n -t Capacity-Tests/${TEST_NAME}/Events-Capacity.jmx -l  ${BASE_PATH}/${NAME_OF_TEST}.jtl -Joutput_folder=${BASE_PATH}/ -Jerrorfile=${BASE_PATH}/ -Jlog -Jconcurrentthreads=${THREADS} -Jduration=14400 > ${BASE_PATH}/jmeterconsoleoutput.txt

cp -r csv-to-html-table-master/* ${BASE_PATH}/
sh ${JMETER_PATH}/jmeter.sh -n -t Capacity-Tests/${TEST_NAME}/PostOpstate.jmx -l  ${BASE_PATH}/${NAME_OF_TEST}.jtl -Joutput_folder=${BASE_PATH}/ -Jerrorfile=${BASE_PATH}/ -Jlog -Jconcurrentthreads=${THREADS} -Jduration=14400 > ${BASE_PATH}/jmeterconsoleoutput.txt

java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-csv "${BASE_PATH}/data/agreegatereport.csv" --input-jtl "${BASE_PATH}/${NAME_OF_TEST}.jtl" --plugin-type AggregateReport

java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-png "${BASE_PATH}/images/RTOT.png" --input-jtl "${BASE_PATH}/${NAME_OF_TEST}.jtl" --plugin-type ResponseTimesOverTime --width 800 --height 600

java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-png "${BASE_PATH}/images/TPS.png" --input-jtl "${BASE_PATH}/${NAME_OF_TEST}.jtl" --plugin-type TransactionsPerSecond --width 800 --height 600

java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-png "${BASE_PATH}/images/HPS.png" --input-jtl "${BASE_PATH}/${NAME_OF_TEST}.jtl" --plugin-type HitsPerSecond --width 800 --height 600

mv  jmeter.log  ${BASE_PATH}

echo ""
echo ""
endTime=$(date -d '+5 hour 30 min' -u +"%Y-%m-%dT%H:%M:%S.%3NZ")

echo "********************************************** "
echo "End Time : ${endTime}"
echo "********************************************** "

echo "End Time : ${endTime}" >> ${BASE_PATH}/testduration.txt

#send message to slack.
sendMessageToSlack ${BASE_PATH}/data/agreegatereport.csv