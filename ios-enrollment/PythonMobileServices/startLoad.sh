#!/bin/bash
cdir=`pwd`
for u in user$1
do
	`cp -f "$cdir"/pkcs7.py "$cdir"/"$u"/pkcs7.py`
	`cp -f "$cdir"/mdm_execute.sh "$cdir"/"$u"/mdm_execute.sh`
	cd "$cdir"/"$u"
	sh mdm_execute.sh
	sleep 1
done
