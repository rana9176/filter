#
# All Rights Reserved.
# Copyright 2013 Symantec Corporation
#
from __future__ import absolute_import
import plistlib
import base64
import subprocess
import settings

from communication import makeRequest, baseUrl
from logging2 import log
import crypto_mdm

class MdmAgent():
    def __init__(self, config_dict):
        self.config_dict = config_dict
        self.command = {}
        self.devices = []

    def do_enroll(self, url):
        r = makeRequest(url, None)
        resp = r.read()
        return resp

    def get_config_url(self, resp):
        search_str = '<key>URL</key>'
        start = resp.find(search_str)
        search_str = '<string>'
        start = resp.find(search_str, start) + len(search_str)
        end = resp.find('</', start)
        return resp[start:end]

    def extract_challenge(self, resp):
        search_str = '<key>Challenge</key>'
        start = resp.find(search_str)
        search_str = '<string>'
        start = resp.find(search_str, start) + len(search_str)
        end = resp.find('</', start)
        return resp[start:end]


    def create_config1(self, challenge,url):
        plist = '<?xml version="1.0" encoding="utf-8"?>'
        plist += '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">'
        plist += '<plist version="1.0">'
        plist += '<dict>'
        plist += '<key>URL</key><string>%s</string>' % url  # https://moco.nuk9
        # .com:444/iosone/Config
        # .aspx?identity=payload
        if challenge:
            plist += '<key>CHALLENGE</key><string>%s</string>' % challenge
        plist += '<key>IMEI</key><string>%s</string>' % self.config_dict['IMEI']  #01 300265 898977 8
        plist += '<key>PRODUCT</key><string>%s</string>' % self.config_dict['PRODUCT']  #iPhone3,1
        plist += '<key>SERIAL</key><string>%s</string>' % self.config_dict['SERIAL']  #652243C4A4S
        plist += '<key>UDID</key><string>%s</string>' % self.config_dict['UDID']  #7582e5bc821af9e50ee612363c4338f54ac0ee82
        plist += '<key>VERSION</key><string>%s</string>' % self.config_dict['VERSION']  #11B511
        plist += '<key>Status</key><string>Acknowledged</string>'
        plist += '<key>InformationType</key><string>OTAInformation</string>'
        plist += '<key>PushSubject</key><string>com.apple.mgmt.odysseymdm</string>'
        #plist += '<key>DeviceID</key><string>%s</string>' % self.config_dict['DeviceId']
        plist += '<key>Topic</key><string>null</string>'
        plist += '<key>MessageResult</key><string>null</string>'
        plist += '<key>Type</key><string>Symantec.Mdm.Library.Apple.AppleServiceLibrary.Information.OTAInformation</string>'
        plist += '</dict>'
        plist += '</plist>'
        return plist


    def create_config(self, challenge,config_url):
        plist = '<?xml version="1.0" encoding="utf-8"?>'
        plist += '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">'
        plist += '<plist version="1.0">'
        plist += '<dict>'
        plist += '<key>URL</key><string>%s</string>' % config_url  # https://moco.nuk9
        # .com:444/iosone/Config
        # .aspx?identity=payload
        if challenge:
            plist += '<key>CHALLENGE</key><string>%s</string>' % challenge
        plist += '<key>IMEI</key><string>%s</string>' % self.config_dict['IMEI']  #01 300265 898977 8
        plist += '<key>PRODUCT</key><string>%s</string>' % self.config_dict['PRODUCT']  #iPhone3,1
        plist += '<key>SERIAL</key><string>%s</string>' % self.config_dict['SERIAL']  #652243C4A4S
        plist += '<key>UDID</key><string>%s</string>' % self.config_dict['UDID']  #7582e5bc821af9e50ee612363c4338f54ac0ee82
        plist += '<key>VERSION</key><string>%s</string>' % self.config_dict['VERSION']  #11B511
        #plist += '<key>DeviceID</key><string>%s</string>' %self.config_dict['DeviceId']
        plist += '<key>DEVICE_NAME</key><string>Nikita iPhone</string>' #% self.config_dict['VERSION']  #11B511
        plist += '<key>UserID</key><string>%s</string>'% settings.user_id#% self.config_dict['VERSION']  #11B511
        plist += '</dict>'
        plist += '</plist>'
        return plist

    def do_config(self, url, config):
        headers = {'Content-Type': 'application/xml'}
        decoded_config = base64.b64decode(config)
        print "URL : ",url
        log('url: %s' %url)
        r = makeRequest(url, decoded_config, None, headers)
        return r.read()
        pass

    # Create a scep request.
    # Currently, uses some hard coded values.
    def create_scep_request(self, subject):
        #/Users/administrator/myopenssl0_9_8y/bin/openssl req -out CSR.csr -key privateKey.key -new -subj "/C=US/O=Symantec Corp/OU=2014-01-29-09-42-45/CN=28bb6ece53d1264a2ff62f30aedd929771f924cb:e31ec853f83770b8878fb5250ab135a480c58f68ffa88a7e365222293c8e8ed9"
        print "creating scep request"
        cmd = [
            '/usr/local/bin/openssl',
            'req',  # Certificate request
            '-out',  # output parameter
            'csr.csr',  # output file name
            '-key',  # private key parameter
            'privateKey.key',  # private key file name
            '-new',  # Um, this makes it new, or something?
            '-subj',  # subject parameter
            subject  # subject of request
        ]

        try:
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except Exception as e:
            print('An exception occurred while running easy_popen - %s' % e)
            settings.errorMessage='Error Message: An exception occurred while running easy_popen - %s' % e
            return None
        result, error = p.communicate()
        #print result
        return result


    def create_pkcs8(self):
        # openssl pkcs8 -topk8 -inform pem -outform der -in privateKey.key -nocrypt > privateKey.pkcs8

        cmd = [
            '/usr/local/bin/openssl',
            'pkcs8',
            '-topk8',
            '-in',
            'privateKey.key',
            '-inform',
            'pem',
            '-outform',
            'der',
            '-nocrypt'
        ]

        try:
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except Exception as e:
            print('An exception occurred while running easy_popen - %s' % e)
            settings.errorMessage='Error Message: An exception occurred while running easy_popen - %s' % e
            return None
        result, error = p.communicate()
        #print result
        return result

    def authenticate(self, checkin_url, topic, encryption_file_name, udid, path):
        message = '<?xml version="1.0" encoding="UTF-8"?>'
        message += '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">'
        message += '<plist version="1.0">'
        message += '<dict>'
        message += '<key>MessageType</key>'
        message += '<string>Authenticate</string>'
        message += '<key>Topic</key>'
        message += '<string>%s</string>' % topic
        message += '<key>UDID</key>'
        message += '<string>%s</string>' % udid
        message += '</dict>'
        message += '</plist>'
        #print 'signing authenticate message - %s' % message
        log('signing authenticate message - %s' % message)

        #mdm_signature = crypto.sign_data(message, '%s.pem' % encryption_file_name, '%s.key' % encryption_file_name, path, True)
        mdm_signature = crypto_mdm.sign_data(message, 'issuedcert.pem', 'device_privatekey', path, True)
        #headers = {'Mdm-Signature': mdm_signature, 'User-Agent': 'MDM-OSX/1.0', 'Content-Type': 'application/x-apple-aspen-mdm-checkin',
        #          'Content-Length': str(len(message))}#, 'Accept': '*/*', 'Accept-Encoding': 'gzip, deflate', 'Accept-Language': 'en-us'}

        #import requests; resp =requests.put(checkin_url, message, headers=headers)
        # print("START MDM SIGNATURE")
        # print(mdm_signature)
        # print("END MDM SIGNATURE")
        # print("START HEADERS")
        # print(headers)
        # print("END HEADERS")
        # print("MESSAGE")
        # print(message)
        #r = makeRequest(checkin_url, message, 'PUT', headers)

        #####################
        # TRY CURL
        #####################
        cmd = [
            '/usr/bin/curl',
            '-v',
	    '--tlsv1.1',
            '-s',
            '-k',
            '-i',
            '-o',
            '%smdm_authenticate.response' % path,
            '-X',
            'PUT',
            checkin_url,
            '-H',
            "Mdm-Signature: %s" % mdm_signature,
            '-H',
            "Proxy-Connection: keep-alive",
            '-H',
            "Accept-Encoding: gzip, deflate",
            '-H',
            "Content-Type: application/x-apple-aspen-mdm-checkin",
            '-H',
            "Accept-Language: en-us",
            '-H',
            '"Accept: */*"',
            '-H',
            "Connection: keep-alive",
            '%s' % "-A" if settings.IS_MAC == "1" else "-A",
            '"User-Agent: %s"' % "MDM-OSX" if settings.IS_MAC == "1" else "MDM",
            '-T',
            '%smdm_authenticate.request' % path

        ]
        log('command: %s' %cmd)
        f = open('%smdm_authenticate.request' % path, 'w')
        f.write(message)
        f.close()

        try:
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except Exception as e:
            print('An exception occurred while running curl AUTHENTICATE - %s' % e)
            log('An exception occurred while running curl AUTHENTICATE - %s' % e)
            settings.errorMessage='Error Message: An exception occurred while running curl AUTHENTICATE - %s' % e
            return None
        result, error = p.communicate()
        return result
        ####################
        ## END CURL
        #####################
        # return r.read() # return if request used

    def token_update(self, checkin_url, topic, encryption_file_name, udid, path):
        message = '<?xml version="1.0" encoding="UTF-8"?>'
        message += '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">'
        message += '<plist version="1.0">'
        message += '<dict>'
        message += '<key>MessageType</key>'
        message += '<string>TokenUpdate</string>'
        message += '<key>Topic</key>'
        message += '<string>%s</string>' % topic
        message += '<key>UDID</key>'
        message += '<string>%s</string>' % udid
        message += '<key>Token</key>'
        message += '<data>47yfV4amvaU4pWXaK6G1cKJw/4HcPHIuC1kVmVHWPgU=</data>' # TODO - add token here #vMaAjhU2YSeQmrcGB/H1K5lZtDvtAEqVonI7NB9l1+k=
        message += '<key>PushMagic</key>'
        message += '<string>%s</string>' % 'D7985FC5-7475-4BF7-AFA6-0AED895DF23E' # TODO - generate 32-char guid #EA1F8011-BC87-435A-8F37-A5CE56AF4A88
        #message += '<key>UnlockToken</key>'
        #message += '<data>%s</data>' % 'TODO - add unlock token'
        message += '</dict>'
        message += '</plist>'
        #print 'signing tokenupdate message - %s' % message
        log('signing tokenupdate message - %s' % message)
        mdm_signature = crypto_mdm.sign_data(message, 'issuedcert.pem', 'device_privatekey', path, True)

        #headers = {'Mdm-Signature': mdm_signature, 'User-Agent': 'MDM-OSX/1.0', 'Content-Type': 'application/x-apple-aspen-mdm-checkin',
        #           'Content-Length': str(len(message))}#, 'Accept': '*/*', 'Accept-Encoding': 'gzip, deflate', 'Accept-Language': 'en-us'}

        # r = makeRequest(checkin_url, message, 'PUT', headers)
        #####################
        # TRY CURL
        #####################
        cmd = [
            '/usr/bin/curl',
            '-v',
	    '--tlsv1.1',
            '-s',
            '-k',
            '-i',
            '-o',
            '%smdm_tokenupdate.response' % path,
            '-X',
            'PUT',
            checkin_url,
            '-H',
            "Mdm-Signature: %s" % mdm_signature,
            '-H',
            "Proxy-Connection: keep-alive",
            '-H',
            "Accept-Encoding: gzip, deflate",
            '-H',
            "Content-Type: application/x-apple-aspen-mdm-checkin",
            '-H',
            "Accept-Language: en-us",
            '-H',
            "Accept: */*",
            '-H',
            "Connection: keep-alive",
            '%s' % "-A" if settings.IS_MAC == "1" else "-A",
            '"User-Agent: %s"' % "MDM-OSX" if settings.IS_MAC == "1" else "MDM",
            '-T',
            '%smdm_tokenupdate.request' % path

        ]
        log('command: %s' %cmd)
        f = open('%smdm_tokenupdate.request' % path, 'w')
        f.write(message)
        f.close()

        try:
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except Exception as e:
            print('An exception occurred while running curl TOKEN UPDATE - %s' % e)
            log('An exception occurred while running curl TOKEN UPDATE - %s' % e)
            settings.errorMessage='Error Message: An exception occurred while running curl TOKEN UPDATE - %s' % e
            return None
        result, error = p.communicate()
        return result

        ####################
        ## END CURL
        #####################
        # return r.read() #return if request is used

    def idle(self):
        pass

    def acknowledge(self, command_udid):
        pass

    #url = baseUrl() + 'appstore/mdmapi1/mdmserver?acid={0}'
    #url = url.format(ACID)
    #def process_commands(self, url):
    def process_commands(self, device_udid, path):
        url = baseUrl() + 'appstore/mdmapi1/mdmserver?acid={0}'
        url = url.format(device_udid)

        if not self.command:
            self.send_idle(url, device_udid, path)

        while self.command:
            self.acknowledge_command(url, device_udid, path)

    def send_idle(self, url, udid, path):
        data = '<?xml version="1.0" encoding="UTF-8"?>'
        data += '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">'
        data += '<plist version="1.0">'
        data += '<dict><key>UDID</key><string>{0}</string><key>Status</key><string>Idle</string></dict></plist>'
        #data = data.format(self.udid)
        data = data.format(udid)                # device udid
        mdm_signature = crypto_mdm.sign_data(data, 'issuedcert.pem', 'device_privatekey', path, True)
        headers = {'Mdm-Signature': mdm_signature, 'Proxy-Connection': 'keep-alive', 'Accept-Encoding': 'gzip, deflate', 'Content-Type': 'application/x-apple-aspen-mdm',
                   'Accept-Language': 'en-us', 'Accept': '*/*','Connection': 'keep-alive', 'User-Agent': 'MDM-OSX/1.0'}
        try:
            r = makeRequest(url, data, 'PUT', headers)
            print 'Response " \n%s'%(r)
            if r:
                resp = r.read()
                log('Idle successful: \nStatus Idle Response : %s' % resp)

                if resp:
                    # Create command from plist in response
                    plist = plistlib.readPlistFromString(resp)
                    self.command = MdmCommand(plist)
                else:
                    log('No response')
            else:
                log('No request')
        except Exception as e:
            log("Error occurred getting commands: %s" % (e.message))
            settings.errorMessage='Error Message: An exception occurred getting commands - %s' % e

    def extract_payload(self, test_device, payload_bin):
        agent_path="/Users/nikita_rathod/Downloads/PythonMobileServices/AppCenterToolsFiles/Device-C38F99631488/"
        f = open(agent_path + "payload.bin", 'w')
        f.write(payload_bin)
        f.close()

        cmd = [
            '/acpt_openssl/bin/openssl',
            'smime',
            '-verify',
            '-inform',
            'der',
            '-noverify',
            '-in',
            '%spayload.bin' % agent_path,
            '-out',
            '%spayload.txt' % agent_path,
        ]

        try:
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except Exception as e:
            print('An exception occurred while running  - %s' % e)
            settings.errorMessage='Error Message: An exception occurred while running popen- %s' % e
            return None
        result, error = p.communicate()
        if result:
            return result
        f = open(agent_path + "payload.txt", 'r')
        payload_txt = f.read()
        f.close()
        return payload_txt

    def acknowledge_command(self, url, udid, path):
        if self.command:
            data = '<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">'
            data += '<plist><dict><key>UDID</key><string>{0}</string><key>Status</key><string>Acknowledged</string><key>CommandUUID</key><string>{1}</string></dict></plist>'
            data = data.format(udid, self.command.uuid)
            mdm_signature = crypto_mdm.sign_data(data, 'issuedcert.pem', 'device_privatekey', path, True)
            headers = {'Mdm-Signature': mdm_signature, 'Proxy-Connection': 'keep-alive', 'Accept-Encoding': 'gzip, deflate', 'Content-Type': 'application/x-apple-aspen-mdm',
                   'Accept-Language': 'en-us', 'Accept': '*/*','Connection': 'keep-alive', 'User-Agent': 'MDM-OSX/1.0'}

            try:
                r = makeRequest(url, data, 'PUT', headers)
                if r:
                    resp = r.read()
                    log('Acknowledge successful: %s' % resp)

                    if resp:
                        # Create command from plist in response
                        plist = plistlib.readPlistFromString(resp)
                        self.command = MdmCommand(plist)
                    else:
                        self.command = None
                else:
                    log('No request')
            except Exception as e:
                log("Error occurred acknowledging command: %s" % (e.message))
                settings.errorMessage='Error Message: An exception occurred acknowledging command  - %s' % e


class MdmCommand():
    def __init__(self, plist):
        log(plist)
        if plist and 'CommandUUID' in plist:
            self.uuid = plist['CommandUUID']
            self.command = plist['Command']
            self.request_type = self.command['RequestType']

