class RestUrlConstantsClass():     
    authenticationUrl=None
    otachallengeUrl=None
    getPSNUrl=None
    getMDRUsersUrl=None
    createConnectTokenUrl=None
    enrollProduct=None
    v1DevicesUrl=None
    v1DevicesUrl=None

    def getr3Url(self):
        return self.r3_url
    
    def setr3Url(self, value):
        self.r3_url=value
     
    def setRestConstants(self):
        self.authenticationUrl = self.r3_url + '/r3_epmp_i/v1/authentication?response_type=token'
        self.otachallengeUrl = self.r3_url + '/r3_epmp_i/epmp-admin/enrollment/otachallengeurl'
        self.getPSNUrl = self.r3_url + '/r3_epmp_i/scsem-admin/v1/products'
        self.getMDRUsersUrl = self.r3_url + '/r3_epmp_i/v1/mdr/users'
        self.createConnectTokenUrl = self.r3_url + '/r3_epmp_i/epmp/v1/connecttokens'
        self.enrollProduct = self.r3_url + '/r3_epmp_i/epmp-admin/enrollment/products'
        self.v1DevicesUrl = self.r3_url + '/r3_epmp_i/scsem/v1/devices'    