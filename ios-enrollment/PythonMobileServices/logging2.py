#
# All Rights Reserved.
# Copyright 2013 Symantec Corporation
#
from __future__ import absolute_import
from datetime import datetime

import settings
import os
import shutil

cwd = os.path.dirname(os.path.abspath(__file__))
logfileDir=cwd + '/logs'
date = datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
latestLogFilePath = logfileDir + '/latestPythonMobileService.log'
f = None

def openFileForLogging():
    
    print "Opening file for logging: " +latestLogFilePath
    global f
    if f is None:
        try:
            f = open(latestLogFilePath, 'w')
        except Exception as e:
            print "Exception: " + e.message   
            log("Exception: %s" %e.message)
            settings.errorMessage = "Error Message: %s"%e.message

def log(msg):
    if settings.LOG_TO_FILE:
        writeLogFile(msg)

def writeLogFile(msg):
    f.write('%s - %s\r\n' % (datetime.now(), msg))

def backupOldLogIFile():
    
    print("Backing up old log file to directory: %s" %logfileDir)
    try:
        if not os.path.exists(logfileDir):
            print "Creating log directory under " + cwd
            os.makedirs(logfileDir)
        if os.listdir(logfileDir):
            backupFileName = logfileDir+'/backup_pythonMobileService_'+date+'.log' 
            shutil.copy(latestLogFilePath, backupFileName)
        else:
            print "Nothing to backup"    
    except Exception as e:
            print "Exception: " + e.message
            settings.errorMessage = "Error Message:  %s"%e.message       
    
def closeLogFile():
    if f is not None:
        f.close()