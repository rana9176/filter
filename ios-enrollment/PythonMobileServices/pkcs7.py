#
# All Rights Reserved.
# Copyright 2013 Symantec Corporation
#
import argparse
import base64
import logging
import os
import plistlib
import sys
import settings
logger = logging.getLogger('aclog')
handler = logging.StreamHandler()
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

def process_argparse(args=None):
    arg_parser = argparse.ArgumentParser(description='Create pkcs7 signatures')
    #arg_parser.add_argument('action', type=str, nargs='?', default=None, help='what to do: create or verify')
    arg_parser.add_argument('-d', '--data', type=str, nargs='?', default=None, help='the data to sign')
    arg_parser.add_argument('-k', '--key', type=str, nargs='?', default=None, help='private key')
    arg_parser.add_argument('-c', '--cert', type=str, nargs='?', default=None, help='client cert')
    arg_parser.add_argument('--cabundle', type=str, nargs='?', default=None, help='ca bundle')
    arg_parser.add_argument('--detached', action="store_true", default=False, help='signature is detached')
    arg_parser.add_argument('--scep-cert-dir', '--scep', type=str, default=None, help="where to find the openscep cacert.pem and cacert0.pem files. If given, causes the created signature to be validated against them.")
    arg_parser.add_argument('--validate', action="store_true", default=False, help="Validate the created signature. If --scep-cert-dir is also given, certificate validation is done.")
    arg_parser.add_argument('-v', '--verbose', action="store_true", default=False, help="verbose printing")
    #arg_parser.add_argument('--pkcs7', type=str, default=None, help="the pkcs7 to verify (if omitted, p7 data is read from stdin")
    return arg_parser

try:
    #noinspection PyUnresolvedReferences
    from ac_common.core.script_utils import ScriptBase
    class Script(ScriptBase):
        def __init__(self, *args, **kwargs):
            super(Script, self).__init__(*args, **kwargs)
            self.arg_parser = process_argparse()

        def run(self, *args):
            args = self.arg_parser.parse_args(args=args)
            process_args(args, verify=True)
            return True
except ImportError:
    pass


def main():
    args = process_argparse().parse_args()
    process_args(args)

def process_args(args, verify=False):
    if not args.key or not args.cert:
        print "ERROR: Missing args"
        return False
    if not os.path.isfile(args.key):
        print "ERROR: keyfile %s does not exist" % args.key
        return False
    if not os.path.isfile(args.cert):
        print "ERROR: certfile %s does not exist" % args.cert
        return False
    if args.cabundle and not os.path.isfile(args.cabundle):
        print "ERROR: cabundle file does not exist" % args.cabundle

    if args.data:
        if not os.path.isfile(args.data):
            print "ERROR: datafile %s does not exist" % args.data
            return False
        data = open(args.data).read()
    else:
        print "Reading data from stdin."
        data = sys.stdin.read()

    # possibly use: openssl cms -sign -in body -out p7cms -signer issuedcert.pem -inkey privateKey.key -outform der

    p7klass = pkcs7(cert=args.cert, ca_chain=args.cabundle, key=args.key, pp=None)
    p7_der = p7klass.pkcs7_sign_content(data, detached=args.detached)
    signature = base64.b64encode(p7_der)
    if args.verbose:
        print "Input data:\n-------\n%s-------" % data
    if args.verbose:
        print "Signature:"
    print signature

    if args.validate:
        if args.scep_cert_dir:
            if not os.path.isdir(args.scep_cert_dir):
                print "ERROR: scep certificate directory %s does not exist or is not a directory." % args.scep_cert_dir
            if args.verbose:
                print "Looking for cacert.pem and cacert0.pem in '%s'" % args.scep_cert_dir
            cacert = os.path.join(args.scep_cert_dir, 'cacert.pem')
            if not os.path.isfile(cacert):
                print "ERROR: Could not find %s" % cacert
                return False
            cacert0 = os.path.join(args.scep_cert_dir, 'cacert0.pem')
            if not os.path.isfile(cacert0):
                print "ERROR: Could not find %s" % cacert0
                return False
        else:
            cacert = None
            cacert0 = None
        scep = pkcs7(cert=cacert, ca_chain=cacert0)

        p7 = scep.pkcs7_from_pem(signature)

        content = data if args.detached else None
        verify = True if cacert0 else False
        try:
            data = scep.verify_signature_pkcs7_and_get_contents(p7, content=content, verify=verify)
            if not data:
                print "ERROR: signature self-validation failed. Unknown error."
                return False
            if args.verbose:
                print "Validated data:\n-------\n%s-------\n" % data

        except Exception as e:
            print "ERROR: signature self-validation failed: %s" % e
            settings.errorMessage="Error Message: signature self-validation failed: %s" % e
            return False

    return True

from M2Crypto import BIO
from M2Crypto import X509
from M2Crypto import SMIME

class pkcs7(object):
    class FileNotFound(Exception):
        pass

    class VerifyFailure(Exception):
        pass

    def __init__(self, cert=None, ca_chain=None, key=None, pp=None):
        """
        @param cert: the certificate path/filename (optional)
        @type cert: str,None
        @param ca_chain: the CA certificate chain filename/path. Optional.
        @type ca_chain: str,None
        @param key: the key filename/path
        @type key: str,None
        @param pp: the passphrase file for the key
        @type pp: str,None
        """
        import os
        if cert and not os.path.exists(cert):
            raise self.FileNotFound(cert)

        if key and not os.path.exists(key):
            raise self.FileNotFound(key)

        if pp and not os.path.exists(pp):
            raise self.FileNotFound(pp)

        if ca_chain and not os.path.exists(ca_chain):
            raise self.FileNotFound(ca_chain)

        self.certfile = cert
        self.keyfile = key
        self.ca_chain_file = ca_chain
        self.pp = pp

    @classmethod
    def get_signer_from_signature(cls, signature):
        try:
            #logger.debug("getting signer from signature")
            p7 = cls.pkcs7_from_pem(signature)
            x509_stack = X509.X509_Stack()
            signers = p7.get0_signers(x509_stack)
            signer = signers[0]
            return signer.as_pem()
        except Exception as e:
            logger.error("API_SIG_SIGNER_404", data={'Exception': e})
            settings.errorMessage="Error Message:  API_SIG_SIGNER_404"
        return None

    def passphrase_callback(self):
        import os

        if not self.pp:
            return None
        if os.path.exists(self.pp):
            try:
                f = open(self.pp, 'r')
                text = f.read()
                f.close()
                pp = str(text.strip())
                return pp
            except Exception as e:
                logger.warn("unable to load the passphrase for the signing cert - %s", e)
                settings.errorMessage="Error Message: unable to load the passphrase for the signing cert - %s", e
                return None
        elif isinstance(self.pp, str):
            return self.pp

    # assuming a single signer, get the issuer string.
    @classmethod
    def get_signer_issuer(cls, p7):
        x509_stack = X509.X509_Stack()
        signers = p7.get0_signers(x509_stack)
        return unicode(signers[0].get_issuer()) if signers else None

    def verify_signature_pkcs7_and_get_contents(self, p7, content=None, verify=True, flags=None):
        if not isinstance(p7, SMIME.PKCS7):
            raise Exception('passed in p7 is not SMIME.PKCS7')

        def bio_from_content(content):
            return BIO.MemoryBuffer(content) if content else None

        if flags is None:
            flags = 0

        cert = X509.load_cert(self.certfile) if self.certfile else None

        s = SMIME.SMIME()

        st = X509.X509_Store()
        if self.ca_chain_file:
            st.load_info(self.ca_chain_file)
        if cert:
            st.add_cert(cert)

        s.set_x509_store(st)

        x509_stack = p7.get0_signers(X509.X509_Stack())
        s.set_x509_stack(x509_stack)

        if not verify:
            flags |= SMIME.PKCS7_NOVERIFY

        bio = None
        try:
            bio = bio_from_content(content)
            contents = s.verify(p7, bio, flags=flags)
        finally:
            if bio:
                bio.close()

        return contents

    @classmethod
    def splitBundle(cls, filename):
        with open(filename, 'r') as filep:
            bundle = filep.read()
        sep = "-----BEGIN CERTIFICATE-----\n"
        return [sep + x for x in bundle.split(sep)[1:]]

    # take content and PKCS#7 wrap it (signed), give it to the caller DER-encoded.
    def pkcs7_sign_content(self, content, detached=False):
        """
        @param content: the content to sign
        @type content: basestring
        @param detached: whether to create a detached signature. Default False
        @type detached: bool

        @return: the DER encoded p7
        @rtype: str
        """
        s = SMIME.SMIME()
        data_bio = BIO.MemoryBuffer(content)
        bundle_certs = self.splitBundle(self.ca_chain_file) if self.ca_chain_file else []
        try:
            if self.keyfile:
                pp = self.passphrase_callback()
                s.load_key(self.keyfile, self.certfile, lambda prompt: pp)
            sk = X509.X509_Stack()
            for crt in bundle_certs:
                sk.push(X509.load_cert_string(crt))
            s.set_x509_stack(sk)
            st = X509.X509_Store()
            if self.ca_chain_file:
                st.load_info(self.ca_chain_file)
            s.set_x509_store(st)
            flags = 0
            if detached:
                flags |= SMIME.PKCS7_DETACHED|SMIME.PKCS7_BINARY
            p7 = s.sign(data_bio, flags=flags)
            if not p7:
                return False

        except Exception as e:
            logger.error("API_PKCS7_SIGN_ERROR", data={'Exception': e})
            settings.errorMessage="Error Message: API_PKCS7_SIGN_ERROR : %s" %e.message
            return False
        finally:
            data_bio.close()

        der_bio = BIO.MemoryBuffer()
        try:
            p7.write_der(der_bio)
            wrapped = der_bio.read()
        except Exception as e:
            logger.error("API_PKCS7_WRITE_DER_ERROR", data={'Exception': e})
            settings.errorMessage="Error Message: API_PKCS7_WRITE_DER_ERROR: %s" %e.message
            return False
        finally:
            der_bio.close()
        return wrapped

    def pkcs7_decrypt(self, pkcs7_der):
        p7 = self.pkcs7_from_der(pkcs7_der)
        return self.pkcs7_decrypt_pem(p7)

    def pkcs7_decrypt_pem(self, p7):
        s = SMIME.SMIME()
        if self.ca_chain_file:
            bundle_certs = self.splitBundle(self.ca_chain_file)
        else:
            bundle_certs = []

        try:
            if self.keyfile:
                pp = self.passphrase_callback()
                s.load_key(self.keyfile, self.certfile, lambda prompt: pp)
            x509 = X509.load_cert(self.certfile)
            sk = X509.X509_Stack()
            sk.push(x509)
            for crt in bundle_certs:
                sk.push(X509.load_cert_string(crt))
            s.set_x509_stack(sk)
            st = X509.X509_Store()
            if (self.ca_chain_file):
                st.load_info(self.ca_chain_file)

            s.set_x509_store(st)
            decrypted = s.decrypt(p7)
            return decrypted
        except Exception as e:
            import traceback
            logger.error("API_PKCS7_DECRYPT_ERROR", data={'Exception': e})
            logger.debug('msg=Exception traceback: %s', traceback.format_exc())
            settings.errorMessage="Error Message: API_PKCS7_DECRYPT_ERRORmsg=Exception traceback: %s", traceback.format_exc()
            return False

    def pkcs7_encrypt_content(self, recipient_certs, content, sign=False):
        from M2Crypto.SMIME import Cipher as SMIMECipher

        if not isinstance(recipient_certs, list):
            if not isinstance(recipient_certs, X509.X509):
                raise Exception('no an x509 cert nor a list of x509 certs')
            recipient_certs = [recipient_certs]
        else:
            for c in recipient_certs:
                if not isinstance(c, X509.X509):
                    raise Exception('list item is not an x509 cert')

        s = SMIME.SMIME()
        if sign:
            pkcs7 = self.pkcs7_sign_content(content)
            if not pkcs7:
                return False
            data_bio = BIO.MemoryBuffer()
            s.write(data_bio, pkcs7)
        else:
            data_bio = BIO.MemoryBuffer(content)

        try:
            sk = X509.X509_Stack()
            for c in recipient_certs:
                sk.push(c)
            s.set_x509_stack(sk)
            s.set_cipher(SMIMECipher('aes_256_cbc'))
            pkcs7 = s.encrypt(data_bio)
            if not pkcs7:
                return False
        except Exception as e:
            import traceback
            logger.error("Could not encrypt pkcs7 content: %s", e)
            logger.debug('msg=Exception traceback: %s', traceback.format_exc())
            settings.errorMessage="Error Message: Could not encrypt pkcs7 content:: %s", e
            return False
        finally:
            data_bio.close()
        return pkcs7

    @classmethod
    def as_der(klass, pkcs7):
        der_bio = BIO.MemoryBuffer()
        try:
            pkcs7.write_der(der_bio)
            pkcs7_der = der_bio.read()
        except Exception as e:
            logger.error("Could not write pkcs7 der: %s", e)
            settings.errorMessage="Error Message: Could not write pkcs7 der: %s", e
            return False
        finally:
            der_bio.close()
        return pkcs7_der

    # take DER-encoded content, convert it to PEM and load it into a PKCS#7 object.
    @classmethod
    def pkcs7_from_der(cls, der_form):
        pem_form = cls.add_header_footer_to_pem(base64.encodestring(der_form))
        raw_data_bio = BIO.MemoryBuffer(pem_form)
        p7 = SMIME.load_pkcs7_bio(raw_data_bio)
        raw_data_bio.close()
        return p7

    @classmethod
    def pkcs7_from_pem(cls, pem_data, already_wrapped=False):
        if not already_wrapped:
            import textwrap
            lines = textwrap.wrap(pem_data,76)
            lines.append('') #trailing newline
            pem_datanl = "\n".join(lines)
        else:
            pem_datanl = pem_data
        pem_form = cls.add_header_footer_to_pem(pem_datanl)
        raw_data_bio = BIO.MemoryBuffer(pem_form)
        p7 = SMIME.load_pkcs7_bio(raw_data_bio)
        raw_data_bio.close()
        return p7

    @classmethod
    def add_header_footer_to_pem(cls, data):
        return ''.join(('-----BEGIN PKCS7-----\n', data, '-----END PKCS7-----'))

if __name__ == "__main__":
    main()
