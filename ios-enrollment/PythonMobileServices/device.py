from __future__ import absolute_import

#
# All Rights Reserved.
# Copyright 2013 Symantec Corporation
#

import json

import crypto_mdm
import random
import string
from mdm import MdmAgent
import settings
import requests
from py4j.java_gateway import JavaGateway, GatewayClient
import os
import datetime
import time
from logging2 import log
from RestUrlConstants import RestUrlConstantsClass


class VirtualDevice():
    r = None

    def __init__(self, id):
        self.id = id  # Identifies the device in the Virtual MDM DB
        self.mdm = MdmAgent(self.get_mdm(settings.IS_MAC))
        self.access_token = ''
        if not os.path.exists(settings.FILE_PATH + settings.DEVICE + "-" + settings.SERIAL_NUMBER):
            os.makedirs(settings.FILE_PATH + settings.DEVICE + "-" + settings.SERIAL_NUMBER)
        self.agent_path = settings.FILE_PATH + settings.DEVICE + "-" + settings.SERIAL_NUMBER + '/'

    def __unicode__(self):
        return unicode('id = %s, hash = %s, deviceID = %s, udid = %s' % (self.id, self.hash, self.device_id, self.udid))

    @property
    def device_id(self):
        return ('0401%s' % str(self.id).zfill(36))

    @property
    def udid(self):
        return self.hash

    @property
    def acid(self):
        return self.hash

    @property
    def device_cert(self):
        return self.device_cert

    @property
    def encryption_cert(self):
        return self.encryption_cert

    @property
    def identity_cert(self):
        return self.identity_cert

    @property
    def token(self):
        return self.token

    @property
    def magic(self):
        return self.magic

    def get_token(self):
        log(
            '**************************************************  Retrieving access token  *******************************************************************************')
        print(
            '**************************************************   Retrieving access token *******************************************************************************')
        token_url = self.r.authenticationUrl
        log('url: %s' % token_url)
        headers = {'Content-Type': 'application/json', 'x-epmp-domain-id': settings.domain_id,
                   'x-epmp-customer-id': settings.customer_id}
        log('headers: %s' % headers)
        print('headers: %s' % headers)
        post_data = {'email': settings.ADMIN_USERNAME, 'password': settings.ADMIN_PASSWORD}
        log('payload: %s' % post_data)
        print('payload: %s' % post_data)
        r = executePostCall(token_url, post_data, headers, 200)
        if (r.status_code is not 200):
            settings.errorMessage = "Error Message: Failed to retrieve authentication token, response code: %s" % r.status_code
            print "Error Message: Failed to retrieve authentication token, response code: %s" % r.status_code
        assert r.status_code == 200, "ERROR: Failed to retrieve authentication token, response code: %s" % r.status_code
        r = json.loads(r.content)
        self.access_token = r['access_token']
        log('response: %s' % r)
        log('access token: %s' % self.access_token)
        print('access token: %s' % self.access_token)
        log(
            '**********************************************************************************************************************************************************')
        print(
            '**********************************************************************************************************************************************************')
        return self.access_token
        pass


    def enroll_mdm(self, r3):
        global r
        self.r = RestUrlConstantsClass()
        self.r.setr3Url(r3)
        self.r.setRestConstants()

        print self.agent_path

        access_token = self.get_token()
        settings.transaction_id = transaction_id_generator()
        print "Setting transaction id to : %s" % settings.transaction_id
        log('Setting transaction id to : %s' % settings.transaction_id)
        print(
            "************************************************** Get MDM Install Splash *******************************************************************************")
        log(
            '************************************************** Get MDM Install Splash *******************************************************************************')

        url = self.r.otachallengeUrl + '?userId=%s&ownershipType=CYOD&transactionId=%s' % (
            settings.user_id, settings.transaction_id)
        log('url: %s' % url)
        print("url: %s" % url)

        headers = {'Content-Type': 'application/json', 'x-epmp-domain-id': settings.domain_id,
                   'x-epmp-customer-id': settings.customer_id, 'Authorization': 'Bearer %s' % access_token}

        r = executeGetCall(url, headers, 200)
        if (r.status_code is not 200):
            settings.errorMessage = "Error Message:  Failed to retrieve OTA challenge url, response code: %s" % r.status_code
            print "Error Message: Failed to retrieve OTA challenge url, response code: %s" % r.status_code
        assert r.status_code == 200, "ERROR: Failed to retrieve OTA challenge url, response code: %s" % r.status_code
        log('response: %s' % r)

        enroll_url = r.content
        log('enroll url: %s' % enroll_url)

        # MDM - Call into the enrollment URL to get configuration information
        print(
            "************************************************** MDM Do Enroll *******************************************************************************")
        log(
            '************************************************** MDM Do Enroll *******************************************************************************')
        resp = self.mdm.do_enroll(enroll_url)
        log('response: %s' % resp)

        config_url = self.mdm.get_config_url(resp)
        log('config url: %s' % config_url)
        print("config url: %s" % config_url)

        index_value = config_url.rfind(';')
        url = config_url[:index_value + 1]
        url = url + "x-epmp-transaction-id=" + settings.transaction_id
        log('config url after appending transaction id : %s' % url)
        print url

        index_value = config_url.find('?')

        config_url = url[:index_value] + "?x-epmp-domain-id=" + settings.domain_id + "&amp;x-epmp-customer-id=" + settings.customer_id + "&amp;domain_id=" + settings.domain_id + "&amp;customer_id=" + settings.customer_id + "&amp;x-epmp-user-id=" + settings.user_id + "&amp;x-epmp-ownership-type=CYOD&amp;x-epmp-transaction-id=" + settings.transaction_id
        log('config url: %s' % config_url)

        # TODO - Combine these two extractions
        # MDM - Extract the challenge from the enrollment response
        log('Extract the challenge from the enrollment response')
        challenge = self.mdm.extract_challenge(resp)
        log('challenge: %s' % challenge)

        # MDM - Create configuration using challenge from enrollment response
        log('Create configuration using challenge from enrollment response')
        config = self.mdm.create_config(challenge, config_url)
        log('config: %s' % config)

        # Create subject for device cert and key
        log('Create subject for device cert and key')
        subject = "/C=US/O=Symantec Corp/CN=%s" % '1234'
        device_cert = 'device_cert'
        device_key = 'device_key'
        issued_cert = 'issuedcert'

        # MDM - Create DEVICE certificate and key
        print("Create Device Certificate and Key")
        log('Create DEVICE certificate and key')
        output = crypto_mdm.create_certificate(self.agent_path, device_cert, device_key, subject)
        log('output: %s' % output)

        # MDM - Sign configuration
        log('MDM - Sign configuration')
        signed_config = crypto_mdm.sign_data(config, '%s.crt' % device_cert, '%s.key' % device_key, self.agent_path)
        log('signed_config: %s' % signed_config)
        if not signed_config:
            print 'Failed to sign config.'
            log('Failed to sign config.')
            settings.errorMessage = 'Error Message: Failed to sign config'
            return False

        # MDM - Submit configuration
        print("Submit MDM Configuration")
        log('Submit MDM Configuration')

        index_value = url.find('?')
        url = url[:index_value] + "?x-epmp-domain-id=" + settings.domain_id + "&x-epmp-customer-id=" + settings.customer_id + "&domain_id=" + settings.domain_id + "&customer_id=" + settings.customer_id + "&x-epmp-user-id=" + settings.user_id + "&x-epmp-ownership-type=CYOD&x-epmp-transaction-id=" + settings.transaction_id

        resp = self.mdm.do_config(url, signed_config)
        log('resp: %s' % resp)
        if not resp:
            print 'Failed to receive SCEP payload from config request.'
            log('Failed to receive SCEP payload from config request.')
            settings.errorMessage = "Error Message: Failed to receive SCEP payload from config request"
            return False

        # MDM - Extract challenge
        log('MDM - Extract challenge')
        challenge = extract_csr_challenge(resp)
        log('challenge: %s' % challenge)
        if not challenge:
            print 'SCEP payload did not contain challenge.'
            log('SCEP payload did not contain challenge.')
            settings.errorMessage = "Error Message: SCEP payload did not contain challenge"
            return False

        # MDM - Extract CN
        log('MDM - Extract CN')
        cn = extract_csr_cn(resp)
        log('CN: %s' % cn)
        if not cn:
            print 'SCEP payload did not contain CN'
            log('SCEP payload did not contain CN')
            settings.errorMessage = "Error Message: SCEP payload did not contain CN"
            return False
        log('extract scep url')
        # scep_url = extract_scep_url(resp)
        scep_url = settings.scep_url
        log('scep_url: %s' % scep_url)

        # create new ENCRYPTION cert with subject
        log('create new ENCRYPTION cert with subject')
        enc_request_name = "device"

        # submit to scep
        log('submit to scep')
        gateway = JavaGateway(GatewayClient(port=settings.PY4J_PORT))
        app = gateway.entry_point
        ret = app.enrollCertificate(scep_url, cn, self.agent_path, enc_request_name, challenge)
        
        # convert received der to pem.
        log('convert received der to pem')
        crypto_mdm.der_to_pem('%s.der' % enc_request_name, '%s.pem' % issued_cert, self.agent_path)

        ######## 2nd Signing ########
        # 2nd response - get identity cert.
        # use that cert to sign second request to config
        # second request
        # MDM - Create configuration using challenge from enrollment response

        log(' ######## 2nd Signing ########')
        log(' # 2nd response - get identity cert.')
        log(' #use that cert to sign second request to config')
        log(' #second request')
        log(' # MDM - Create configuration using challenge from enrollment response')
        config = self.mdm.create_config1(None, config_url)
        log('config: %s' % config)
        print("MDM Sign Configuration")
        log('MDM Sign Configuration')
        # MDM - Sign configuration
        # signed_config = self.mdm.sign_config(config, 'device_cert.crt', 'device_cert.key')
        signed_config = crypto_mdm.sign_data(config, '%s.pem' % issued_cert, 'device_privatekey', self.agent_path)
        log('signed_config: %s' % signed_config)
        if not signed_config:
            print 'Failed to sign config.'
            log('Failed to sign config')
            settings.errorMessage = "Error Message: Failed to sign config"
            return False

        print("Submit MDM Configuration")
        log('Submit MDM Configuration')
        # MDM - Submit configuration
        resp = self.mdm.do_config(url, signed_config)
        log('resp: %s' % resp)
        time.sleep(5)
        # print resp
        if not resp:
            print 'Failed to receive mdm profile.'
            log('ERROR: Failed to receive mdm profile.')
            settings.errorMessage = "Error Message: Failed to receive mdm profile"
            return False

        # MDM - Extract Encrypted Payload Content
        log('MDM - Extract Encrypted Payload Content')
        enc_payload = extract_encrypted_payload(resp)
        log('enc_payload: %s' % enc_payload)

        from pkcs7 import pkcs7
        import base64

        p7 = pkcs7(cert=self.agent_path + '%s.pem' % issued_cert, key=self.agent_path + 'device_privatekey')
        log('p7: %s' % p7)

        mdm_profile = p7.pkcs7_decrypt(base64.b64decode(enc_payload))
        log(
            '============================================================== Decrypted mdm profile: ===============================================================================')
        log(mdm_profile)
        log(
            '=====================================================================================================================================================================')
        print(
            '============================================================ Decrypted mdm profile: ===============================================================================')
        print(mdm_profile)
        print(
            '===================================================================================================================================================================')
        #################################################################################################################
        print("MDM - Extract Checkin URL")
        log('MDM - Extract Checkin URL')
        # input("Press Enter to continue...")
        # MDM - Extract Checkin URL
        checkin_url = extract_checkin_url(mdm_profile)
        index_value = checkin_url.find('?')
        checkin_url = checkin_url[:index_value] + "?x-epmp-domain-id=" + settings.domain_id + "&x-epmp-customer-id=" + settings.customer_id + "&domain_id=" + settings.domain_id + "&customer_id=" + settings.customer_id + "&x-epmp-user-id=" + settings.user_id + "&x-epmp-ownership-type=CYOD&x-epmp-transaction-id=" + settings.transaction_id
        print "Checkin URL", checkin_url
        log('Checkin URL: %s' % checkin_url)
        # MDM - Extract Server URL
        server_url = extract_server_url(mdm_profile)
        print "Server URL", server_url
        log('Server URL: %s' % server_url)
        # MDM - Extract Topic
        log('MDM - Extract Topic')
        topic = extract_topic(mdm_profile)
        print "topic", topic
        log('topic: %s' % topic)
        try:
            # MDM - Create DeviceInfo.txt for MDM Client
            log('MDM - Create DeviceInfo.txt for MDM Client')
            self.save_device_info(server_url[8:])

            # MDM - Create, sign and send Authenticate message to Checkin
            log('MDM - Create, sign and send Authenticate message to Checkin')
            resp = self.mdm.authenticate(checkin_url, topic, enc_request_name, settings.UDID, self.agent_path)
            time.sleep(5)

            # MDM - Create, sign and send TokenUpdate message to Checkin
            log('MDM - Create, sign and send TokenUpdate message to Checkin')
            resp = self.mdm.token_update(checkin_url, topic, issued_cert, settings.UDID, self.agent_path)
            time.sleep(5)

            result = False
            if settings.IS_MAC == "0":
                enrolled_device_id = self.get_deviceId(settings.SERIAL_NUMBER)
                log('Enrolled IOS device id: %s' % enrolled_device_id)
                print("enrolled_device_id:%s" % enrolled_device_id)

            if settings.IS_MAC == "1":
                log('MAC enrollment')
                enrolled_device_id = self.enrollment_products()
                if enrolled_device_id is False:
                    print "Exception occurred while enrolling MAC device"

                else:
                    result = True
                    log('Enrolled MAC device id: %s' % enrolled_device_id)
                    print("enrolled_device_id:%s" % enrolled_device_id)
                    self.post_opstate(enrolled_device_id)

            settings.UDID = ''
            settings.SERIAL_NUMBER = ''
            return result
        except Exception as e:
            print e.message
            self.message = 'Error: %s' % e.message
            return result
        '''
        TODO - generate new request based on SCEP Payload data in mdm profile.
        # MDM - Extract challenge
        challenge = extract_csr_challenge(resp)

        # MDM - Extract CN
        cn = extract_csr_cn(resp)

        # Create subject
        subject = "/C=US/O=Symantec Corp/CN=%s" % cn

        #create new cert with subject
        id_request_name = 'identity'
        crypto_mdm.create_certificate(id_request_name, subject)

        #create csr and private key from that cert
        crypto_mdm.create_csr(id_request_name, subject, challenge)

        # convert key file to pkcs8
        crypto_mdm.create_pkcs8(id_request_name)
        '''


    def get_deviceId(self, serial_number):
        log('get enrolled device id')
        print('get enrolled device id')
        device_url = settings.r3_url + '/r3_epmp_i/v1/mdr/devices?where=hw.serial%3D%27' + serial_number + "%27"
        log('url: %s' % device_url)
        headers = {'Content-Type': 'application/json', 'x-epmp-domain-id': settings.domain_id,
                   'x-epmp-customer-id': settings.customer_id, 'Authorization': 'Bearer %s' % self.access_token}
        # resp = requests.get(device_url, headers=headers)
        resp = executeGetCall(device_url, headers, 200)
        if (resp.status_code is not 200):
            settings.errorMessage = "Error Message: Failed to retrieve enrolled device id , response code: %s" % resp.status_code
            print "Error Message: Failed to retrieve enrolled device id , response code: %s" % resp.status_code
        assert resp.status_code == 200, "Failed to retrieve enrolled device id , response code: %s" % resp.status_code
        r = json.loads(resp.content)
        log('response : %s' % r)
        device_id = r[0]['id']
        return device_id

    def get_psn(self):
        token_url = self.r.getPSNUrl
        log("retrieving PSN")
        print("retrieving PSN")
        log("URL: %s" % token_url)
        print("url: %s" % token_url)
        headers = {'Content-Type': 'application/json', 'x-epmp-domain-id': settings.domain_id,
                   'x-epmp-customer-id': settings.customer_id, 'Authorization': 'Bearer %s' % self.access_token}
        # resp = requests.get(token_url, headers=headers)
        resp = executeGetCall(token_url, headers, 200)
        if (resp.status_code is not 200):
            settings.errorMessage = "Error Message: Failed to retrieve PSN , response code: %s" % resp.status_code
            print "Error Message: Failed to retrieve PSN , response code: %s" % resp.status_code
        assert resp.status_code == 200, "ERROR: Failed to retrieve PSN, response code: %s" % resp.status_code
        print('==================')
        print('response code: %s' % resp.status_code)
        print('==================')
        r = json.loads(resp.content)
        log('response: %s' % r)
        psn = r['Licenses'][0]['psn']
        log("PSN: %s" % psn)
        print("PSN: %s" % psn)
        return psn

    def get_olp_account_id(self):
        token_url = self.r.getMDRUsersUrl + '/%s' % settings.user_id
        log("retrieving olp account id")
        print("retrieving olp account id")
        log("url: %s" % token_url)
        print("url: %s" % token_url)
        headers = {'Content-Type': 'application/json', 'x-epmp-domain-id': settings.domain_id,
                   'x-epmp-customer-id': settings.customer_id, 'Authorization': 'Bearer %s' % self.access_token}
        # resp = requests.get(token_url, headers=headers)
        resp = executeGetCall(token_url, headers, 200)
        if (resp.status_code != 200):
            settings.errorMessage = "Error Message: Failed to retrieve olpaccount id, response code: %s" % resp.status_code
            print "Error Message: Failed to retrieve olpaccount id, response code: %s" % resp.status_code
        assert resp.status_code == 200, "ERROR: Failed to retrieve olpaccount id, response code: %s" % resp.status_code
        r = json.loads(resp.content)
        log('response: %s' % r)
        olp_account_id = r['olp_account_id']
        log("olp_account_id: %s" % olp_account_id)
        print("olp_account_id: %s" % olp_account_id)
        return olp_account_id

    def create_connect_token(self):
        log(
            '************************************************** Creating connect token *******************************************************************************')
        print(
            '************************************************** Creating connect token *******************************************************************************')
        token_url = self.r.createConnectTokenUrl
        log('URL: %s' % token_url)
        print('URL: %s' % token_url)
        headers = {'Content-Type': 'application/json', 'x-epmp-domain-id': settings.domain_id,
                   'x-epmp-customer-id': settings.customer_id, 'Authorization': 'Bearer %s' % self.access_token}
        post_data = {"psn": self.get_psn(), "requestingNortonId": self.get_olp_account_id(),
                     "tokenPurposeList": [{"purpose": "MDRBIND", "limitCount": 1},
                                          {"purpose": "ACTIVATION", "limitCount": 1}],
                     "tokenAttributeList": [{"key": "MDRBIND.OPEN.DOMAIN_ID", "value": settings.domain_id},
                                            {"key": "ASSOCIATE_TO_USER", "value": "true"},
                                            {"key": "PACKAGE_TYPE", "value": "MAC"},
                                            {"key": "MDRBIND.OPEN.ENROLLMENT_URL",
                                             "value": "%s/r3_epmp_i/epmp-admin/enrollment/products" % settings.r3_url},
                                            {"key": "MDRBIND.OPEN.CUSTOMER_ID", "value": settings.customer_id},
                                            {"key": "TRANSACTION_ID", "value": settings.transaction_id},
                                            {"key": "OWNERSHIP_TYPE", "value": "CYOD"}]}
        log('post data for creating connect token: \n  %s' % post_data)
        print('post data for creating connect token: \n %s' % post_data)
        # resp = requests.post(token_url, data=json.dumps(post_data), headers=headers)
        resp = executePostCall(token_url, post_data, headers, 201)
        log(
            ' *************************************************************************************************************************************************************')
        print(' *****************************************************************************')
        if (resp.status_code != 201):
            settings.errorMessage = "Error Message:  Failed to create connect token, response code: %s" % resp.status_code
            print "Error Message: Failed to create connect token, response code: %s" % resp.status_code
        assert resp.status_code == 201, "ERROR: Failed to create connect token, response code: %s" % resp.status_code
        r = json.loads(resp.content)
        log('response: %s' % r)
        connect_token = r['connectTokenId']
        log('connect token: %s' % connect_token)
        return connect_token

    def enrollment_products(self):
        try:
            log(
                '************************************************** Enrolling MAC *******************************************************************************')
            print(
                '************************************************** Enrolling MAC *******************************************************************************')
            token_url = self.r.enrollProduct
            log('enrollment url: %s' % token_url)
            headers = {'Content-Type': 'application/json', 'x-epmp-domain-id': settings.domain_id,
                       'x-epmp-customer-id': settings.customer_id, 'Authorization': 'Bearer %s' % self.access_token}
            post_data = {
                "connectToken": self.create_connect_token(),
                "olpEndpointId": settings.SERIAL_NUMBER,
                "osVersion": "10.10",
                "osName": "Mac OS X",
                "hardwareSerialNumber": settings.SERIAL_NUMBER
            }
            log('post data: %s' % post_data)
            print('enrollment url: %s' % token_url)
            print('post data: %s' % post_data)
            # resp = requests.post(token_url, data=json.dumps(post_data), headers=headers)
            resp = executePostCall(token_url, post_data, headers, 201)
            if (resp.status_code != 201):
                settings.errorMessage = "Error Message: MAC enrollment call failed with response code: %s" % resp.status_code
                print "Error Message:  MAC enrollment call failed with response code: %s" % resp.status_code
            assert resp.status_code == 201, "ERROR: MAC enrollment call failed with response code: %s" % resp.status_code
            r = json.loads(resp.content)
            log('response: %s' % r)
            device_id = r['device_id']
            log(
                ' *********************************************************************************************************************************************************')
            print(
                ' *******************************************************************************************************************************************************')
            return device_id
        except Exception as e:
            print e.message
            settings.errorMessage = "Error Message:  %s" % e.message
            return False

    def post_opstate(self, device_id):
        log(
            '************************************************** Posting opstate *******************************************************************************')
        print(
            '************************************************** posting opstate *******************************************************************************')
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d")
        timestamp += "T07:08:53.203Z"
        log('timestamp: %s' % timestamp)
        token_url = self.r.v1DevicesUrl + '/%s/opstate' % device_id
        log('post opstate url: %s' % token_url)
        print('post opstate url: %s' % token_url)
        headers = {'Content-Type': 'application/json', 'x-epmp-domain-id': settings.domain_id,
                   'x-epmp-customer-id': settings.customer_id, 'Authorization': 'Bearer %s' % self.access_token}
        post_data = {
            "attributes": {
                "schemaversion": 1,
                "timestamp": timestamp
            },
            "client": {
                "agent_version": "7.0",
                "device_model_name": "Macmini",
                "device_model_number": "Macmini6,2",
                "device_name": "Nikita's Mac - %s" % settings.SERIAL_NUMBER,
                "device_serial_number": settings.SERIAL_NUMBER,
                "os_name": "Mac OS X",
                "os_version": "10.9.5",
                "timezone_offset": 5,
                "deviceid": device_id,
                "id": "2000",
                "licenseseatid": "A047D631-1FC5-4227-A7C7-48C725D30BEF",
                "ipaddresses": [
                    "fe80::e4d:e9ff:fea3:d7bb",
                    "10.210.152.116",
                    "169.254.43.189",
                    "192.168.2.1"
                ],
                "overallhealth": {
                    "state": {
                        "id": 0
                    }
                },
                "policy": {
                    "appliedstatus": {
                        "applied": 1
                    },
                    "id": "{AA9A2B01-FDA6-490F-9EC8-D72603D2C1C4}",
                    "version": 19
                },
                "version": "7.0f77"
            },
            "features": [
                {
                    "configuration": {
                        "timebox": 0,
                        "enabled": True
                    },
                    "health": {
                        "healthvalue": 0
                    },
                    "properties": {
                        "id": "{74C2542F-ED39-429E-A262-D47DF1D0287A}",
                        "configownership": {
                            "timebox": 3,
                            "enabled": 0
                        },
                        "owningpolicytype": 0,
                        "serialnumber": "0x000000000000002a",
                        "lasterror": 0,
                        "name": "autoprotect",
                        "state": 1,
                        "version": "",
                        "timestamp": "2015-06-15T11:36:32.316Z"
                    },
                    "runtime": {
                        "apstate": 0,
                        "unresolvedthreats": []
                    }
                },
                {
                    "configuration": {},
                    "health": {
                        "healthvalue": 0
                    },
                    "properties": {
                        "id": "{56612773-AD74-46D4-90C3-70CCE64D7864}",
                        "serialnumber": "0x0000000000000033",
                        "lasterror": 0,
                        "name": "defs",
                        "state": 3,
                        "version": "",
                        "timestamp": timestamp
                    },
                    "runtime": {
                        "engineloaded": True,
                        "currentdefsdate": "2015-06-16T00:00:00.000+05:30",
                        "effectivedefsdate": "2015-06-16T00:00:00.000+05:30"
                    }
                },
                {
                    "configuration": {
                        "scancompressed": True,
                        "scannetworkdrives": False,
                        "enablescannotifications": True
                    },
                    "health": {
                        "healthvalue": 0
                    },
                    "properties": {
                        "id": "{EB9A9A6C-01E1-4FCE-B932-B725721E456C}",
                        "configownership": {
                            "scancompressed": 0,
                            "scannetworkdrives": 0,
                            "enablescannotifications": 0,
                            "scheduledscans": 0
                        },
                        "owningpolicytype": 0,
                        "serialnumber": "0x000000000000003a",
                        "lasterror": 0,
                        "name": "scans",
                        "state": 1,
                        "version": "",
                        "timestamp": timestamp
                    },
                    "runtime": {
                        "lastrun": timestamp
                    }
                },
                {
                    "configuration": {
                        "timebox": 0,
                        "enablenotifications": True,
                        "defaultpolicy": 1,
                        "allowtrusted": True,
                        "enabled": True
                    },
                    "health": {
                        "healthvalue": 0
                    },
                    "properties": {
                        "id": "{DB6E80CB-171D-48B6-9598-44D1B0057CC4}",
                        "configownership": {
                            "timebox": 3,
                            "enablenotifications": 0,
                            "defaultpolicy": 0,
                            "allowtrusted": 0,
                            "enabled": 0
                        },
                        "owningpolicytype": 0,
                        "serialnumber": "0x0000000000000018",
                        "lasterror": 0,
                        "name": "applicationblocking",
                        "state": 1,
                        "version": "",
                        "timestamp": "2015-06-15T11:36:18.548Z"
                    },
                    "runtime": {}
                },
                {
                    "configuration": {
                        "timebox": 0,
                        "enabled": False
                    },
                    "health": {
                        "healthvalue": 4
                    },
                    "properties": {
                        "id": "{0E9F307E-A59C-4361-A577-6722D9308B29}",
                        "configownership": {
                            "timebox": 3,
                            "enabled": 4
                        },
                        "owningpolicytype": 4,
                        "serialnumber": "0x000000000000003b",
                        "lasterror": 0,
                        "name": "firewall",
                        "state": 1,
                        "version": "",
                        "timestamp": timestamp
                    },
                    "runtime": {}
                },
                {
                    "configuration": {
                        "submissionenabled": True
                    },
                    "health": {
                        "healthcomponents": {
                            "defs": 0,
                            "autoprotect": 0,
                            "firewall": 4,
                            "ips": 0
                        },
                        "healthvalue": 0
                    },
                    "properties": {
                        "id": "{776E16AD-0BA5-406F-AFE1-FE51FB11C8A0}",
                        "configownership": {
                            "submissionenabled": 0
                        },
                        "owningpolicytype": 0,
                        "serialnumber": "0x000000000000003c",
                        "lasterror": 0,
                        "name": "product",
                        "state": 1,
                        "version": "",
                        "timestamp": timestamp
                    },
                    "runtime": {}
                },
                {
                    "configuration": {
                        "timebox": 0,
                        "enablenotifications": True,
                        "enabled": True
                    },
                    "health": {
                        "healthvalue": 0
                    },
                    "properties": {
                        "id": "{FC8F9950-4738-40A8-81C6-972698355451}",
                        "configownership": {
                            "timebox": 0,
                            "enablenotifications": 0,
                            "enabled": 0
                        },
                        "owningpolicytype": 0,
                        "serialnumber": "0x0000000000000037",
                        "lasterror": 0,
                        "name": "ips",
                        "state": 1,
                        "version": "",
                        "timestamp": timestamp
                    },
                    "runtime": {
                        "engineloaded": 1,
                        "ipsstate": 0,
                        "engineversion": "7.2.0r131"
                    }
                },
                {
                    "configuration": {},
                    "health": {
                        "healthvalue": 2
                    },
                    "properties": {
                        "id": "{92E75AB9-7FAA-47B7-99F8-80A9B48F2D7F}",
                        "serialnumber": "0x0000000000000038",
                        "lasterror": 0,
                        "name": "ipsdefs",
                        "state": 3,
                        "version": "",
                        "timestamp": timestamp
                    },
                    "runtime": {
                        "effectivedefsdate": "2046-06-16T00:00:00.000+05:30",
                        "currentdefsdate,": "2046-06-16T00:00:00.000+05:30"
                    }
                },
                {
                    "configuration": {},
                    "health": {
                        "healthvalue": 0
                    },
                    "properties": {
                        "id": "{95208D17-5926-4A51-9038-D85178D47061}",
                        "configownership": {
                            "exclusions": 0
                        },
                        "owningpolicytype": 0,
                        "serialnumber": "0x000000000000001e",
                        "lasterror": 0,
                        "name": "exclusions",
                        "state": 1,
                        "version": "",
                        "timestamp": "2015-06-15T11:36:19.832Z"
                    },
                    "runtime": {}
                },
                {
                    "configuration": {},
                    "properties": {
                        "id": "{A2010ED8-C1FC-450D-A475-041B4BEFF0D2}",
                        "serialnumber": "0x000000000000002f",
                        "lasterror": 0,
                        "name": "entitlement",
                        "state": 1,
                        "version": "",
                        "timestamp": "2015-06-15T11:36:45.575Z"
                    },
                    "runtime": {}
                },
                {
                    "configuration": {
                        "enabled": True
                    },
                    "health": {
                        "healthvalue": 0
                    },
                    "properties": {
                        "id": "{4ED92162-4361-4AEF-8F68-56B2A597772B}",
                        "configownership": {
                            "enabled": 0
                        },
                        "owningpolicytype": 0,
                        "serialnumber": "0x0000000000000014",
                        "lasterror": 0,
                        "name": "deepsightdownload",
                        "state": 1,
                        "version": "",
                        "timestamp": "2015-06-15T11:36:18.548Z"
                    },
                    "runtime": {}
                },
                {
                    "configuration": {
                        "enabled": True
                    },
                    "health": {
                        "healthvalue": 0
                    },
                    "properties": {
                        "id": "{7A3837CA-2D22-480E-91F3-5C64A27BC0DB}",
                        "configownership": {
                            "enabled": 0
                        },
                        "owningpolicytype": 0,
                        "serialnumber": "0x0000000000000016",
                        "lasterror": 0,
                        "name": "deepsightsubmission",
                        "state": 1,
                        "version": "",
                        "timestamp": "2015-06-15T11:36:18.548Z"
                    },
                    "runtime": {}
                }
            ]
        }
        log('post data: %s' % post_data)
        # resp = requests.post(token_url, data=json.dumps(post_data), headers=headers)
        resp = executePostCall(token_url, post_data, headers, 200)
        log(
            '**************************************************************************************************************************************************************')
        print(
            '************************************************************************************************************************************************************')
        if (resp.status_code != 200):
            settings.errorMessage = "Error Message: Failed to post opstate, response code: %s" % resp.status_code
            print "Error Message: Failed to post opstate, response code: %s" % resp.status_code
        assert resp.status_code == 200, "Failed to post opstate, response code: %s" % resp.status_code
        return resp

    def save_device_info(self, server_url):
        device_info = '<key>DeviceName</key>\n'
        device_info = device_info + '<string>%s</string>\n' % self.get_devicename()
        device_info = device_info + '<key>UDID</key>\n'
        device_info = device_info + '<string>%s</string>\n' % self.get_udid()
        device_info = device_info + '<key>ServerName</key>\n'
        device_info = device_info + '<string>%s</string>\n' % server_url

        f = open('%sDeviceInfo.txt' % self.agent_path, 'w')
        f.write(device_info)
        f.close()

    def get_aca(self):
        print self.device_id
        return dict(platform='ios',
                    device_class='phone',
                    icon_px=72,
                    sdk_version=1,
                    version='11B554a',
                    serial_number='C38F99631488',
                    is_rooted='false',
                    product_string='iPhone3,1',
                    version_string='7.0',
                    customer_email='test@symantec.com',
                    phone_id='',
                    mac_address=self.format_mac(),
                    push_registration_id='',
                    is_mdm_enabled='true',
                    product_name='',
                    model_number='',
                    manufacturer='Apple',
                    IMEI='',
                    udid='c500cf3962b26495818567ce950c7g3131153760',
                    user=self.get_user(),
                    name=self.get_devicename(),
                    core_device_id=self.device_id,
                    first_time='true'
                    )

    def get_mdm(self, is_mac):
        if (is_mac == "1"):
            return dict(URL='https://' + settings.HOSTNAME + ':444/iosone/Config.aspx?identity=payload',
                        IMEI='01 300265 898977 8',
                        PRODUCT='MacBookAir5,2',
                        SERIAL=self.get_mac_serial_number(),  # '652243C4B5F',
                        UDID=self.get_mac_udid(),
                        DEVICE_NAME='Nikitas Mac',
                        VERSION='10.10'
                        )
        else:
            return dict(URL='https://' + settings.HOSTNAME + ':444/iosone/Config.aspx?identity=payload',
                        IMEI='01 300265 898977 8',
                        PRODUCT='iPhone3,1',
                        SERIAL=self.get_serial_number(),  # '652243C4B5F',
                        UDID=self.get_udid(),
                        DEVICE_NAME='Nikitas iPhone',
                        VERSION='11B511'
                        )

    def get_mac_serial_number(self):
        if (settings.SERIAL_NUMBER == ''):
            random_id = ''.join(random.choice(string.digits) for _ in range(7))
            settings.SERIAL_NUMBER = "C07KF" + random_id
            print settings.SERIAL_NUMBER
            return settings.SERIAL_NUMBER
        else:
            return settings.SERIAL_NUMBER

    def get_mac_udid(self):
        if (settings.UDID == ''):
            random_id = ''.join(random.choice(string.digits) for _ in range(12))
            settings.UDID = "FB5C2F8C-D1E1-5E89-9D37-" + random_id
            print settings.UDID
            return settings.UDID
        else:
            return settings.UDID

    def format_mac(self):
        mac = str(self.id).zfill(12)
        newmac = self.insert(mac, ":", 2)
        newmac = self.insert(newmac, ":", 5)
        newmac = self.insert(newmac, ":", 8)
        newmac = self.insert(newmac, ":", 11)
        newmac = self.insert(newmac, ":", 14)
        return newmac

    def get_serial_number(self):
        if (settings.SERIAL_NUMBER == ''):
            random_id = ''.join(random.choice(string.digits) for _ in range(7))
            settings.SERIAL_NUMBER = "C38F9" + random_id
            print settings.SERIAL_NUMBER
            return settings.SERIAL_NUMBER
        else:
            return settings.SERIAL_NUMBER

    def get_udid(self):
        # return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(40))
        if (settings.UDID == ''):
            random_id = ''.join(random.choice(string.digits) for _ in range(10))
            settings.UDID = settings.DEVICE_PREFIX + random_id
            print settings.UDID
            return settings.UDID
        else:
            return settings.UDID
            # return settings.DEVICE_PREFIX + '%s' % str(self.id).zfill(10)

    def get_user(self):
        if settings.LEADING_ZEROS == 1:
            return settings.DEVICE + '%s' % str(self.id).zfill(7)
        else:
            return settings.DEVICE + '%s' % str(self.id)

    def get_devicename(self):
        if settings.IS_MAC == "1":
            return 'Nikita\'s Mac - %s' % settings.SERIAL_NUMBER
        else:
            return 'Nikita\'s iPhone- %s' % settings.SERIAL_NUMBER

    def insert(self, original, new, pos):
        '''Inserts new inside original at pos.'''
        return original[:pos] + new + original[pos:]


# TODO - Use XML parsing, rather than whatever this is.
def extract_csr_challenge(payload):
    search_str = '<key>Challenge</key>'
    start = payload.find(search_str)
    search_str = '<string>'
    start = payload.find(search_str, start) + len(search_str)
    end = payload.find('</', start)
    return payload[start:end]


# TODO - Use XML parsing, rather than whatever this is.
def extract_csr_cn(payload):
    search_str = '<string>CN</string>'
    start = payload.find(search_str)
    search_str = '<string>'
    start = payload.find(search_str, start + 19) + len(search_str)
    end = payload.find('</', start)
    return payload[start:end]


def extract_scep_url(payload):
    search_str = '<key>URL</key>'
    start = payload.find(search_str)
    search_str = '<string>'
    start = payload.find(search_str, start) + len(search_str)
    end = payload.find('</', start)
    return payload[start:end]


# TODO - create class to read in mdm profile. class should have properties for checkin, server and topic.
def extract_encrypted_payload(payload):
    search_str = '<key>EncryptedPayloadContent</key>'
    start = payload.find(search_str)
    search_str = '<data>'
    start = payload.find(search_str, start) + len(search_str)
    end = payload.find('</', start)
    return payload[start:end]


def extract_checkin_url(profile):
    search_str = '<key>CheckInURL</key>'
    start = profile.find(search_str)
    search_str = '<string>'
    start = profile.find(search_str, start) + len(search_str)
    end = profile.find('</', start)
    return profile[start:end]


def extract_server_url(profile):
    search_str = '<key>ServerURL</key>'
    start = profile.find(search_str)
    search_str = '<string>'
    start = profile.find(search_str, start) + len(search_str)
    end = profile.find('</', start)
    return profile[start:end]


def extract_topic(profile):
    search_str = '<key>Topic</key>'
    start = profile.find(search_str)
    search_str = '<string>'
    start = profile.find(search_str, start) + len(search_str)
    end = profile.find('</', start)
    return profile[start:end]


def transaction_id_generator(size=36, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def executePostCall(url, post_data, headers, expectedResponseCode):
    loopCount = 0
    while (loopCount < settings.retry_count):
        r = requests.post(url, data=json.dumps(post_data), headers=headers)
        log('response code: %s' % r.status_code)
        print('==================')
        print('response code: %s' % r.status_code)
        print('==================')
        log('==================')
        log('response code: %s' % r.status_code)
        log('==================')
        if ((r.status_code) != expectedResponseCode):
            loopCount = loopCount + 1
            print "Retrying........attempt no. is : %s" % loopCount
            log("Retrying........attempt no. is : %s" % loopCount)
            time.sleep(5)
        else:
            break
    return r


def executeGetCall(url, headers, expectedResponseCode):
    loopCount = 0
    while (loopCount < settings.retry_count):
        r = requests.get(url, headers=headers)
        log('response code: %s' % r.status_code)
        print('==================')
        print('response code: %s' % r.status_code)
        print('==================')
        log('==================')
        log('response code: %s' % r.status_code)
        log('==================')
        if ((r.status_code) != expectedResponseCode):
            loopCount = loopCount + 1
            print "Retrying........attempt no. is : %s" % loopCount
            log("Retrying........attempt no. is : %s" % loopCount)
            time.sleep(5)
        else:
            break
    return r
