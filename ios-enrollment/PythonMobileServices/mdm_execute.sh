#!/bin/bash 
START_TIME=$SECONDS
function mdmrequest {
mdmcommand=$1
servername=`cat ./DeviceInfo.txt | grep 'ServerName' -A1 | grep '<string>' | sed 's/^.*<string>//;s/<\/string>.*$//'`
serverversion=`cat ./DeviceInfo.txt | grep 'ServerVersion' -A1 | grep '<string>' | sed 's/^.*<string>//;s/<\/string>.*$//' | tail -n 1`
curl -s -k -i -o mdm_command.response -X PUT "https://$servername/appstore/mdmapi1/mdmserver" -H "Host: $servername" -H "Mdm-Signature: `python ./pkcs7.py -d "mdm_$mdmcommand.request" -k encryption.key -c issuedcert.pem --detached --validate`" -H "Proxy-Connection: keep-alive" -H "Accept-Encoding: gzip, deflate" -H "Content-Type: application/x-apple-aspen-mdm" -H "Accept-Language: en-us" -H "Accept: */*" -H "Connection: keep-alive" -H "User-Agent: MDM/1.0" -T "mdm_$mdmcommand.request"	
}
function processcommands {
	mdm_commandid=$1
	deviceid=$2
	mdm_requesttype=$3
	pservername=$4
	core_device_id=$5
#	echo "COMMANDID $mdm_commandid DEVICEID $deviceid REQ TYPE $mdm_requesttype"
if [ "$mdm_requesttype" = "CertificateList" ]
	then
	mdm_certificatelistrequest="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
	<plist version=\"1.0\">
	<dict>
		<key>CertificateList</key>
		<array>
			<dict>
				<key>CommonName</key>
				<string>OSI-DEV-CA</string>
				<key>Data</key>
				<data>
				MIIDhjCCAm6gAwIBAgIQDU8LOHX+Y4NDk9MisUKg2jANBgkqhkiG
				9w0BAQUFADBLMRMwEQYKCZImiZPyLGQBGRYDY29tMR8wHQYKCZIm
				iZPyLGQBGRYPb2R5c3NleXNvZnR3YXJlMRMwEQYDVQQDEwpPU0kt
				REVWLUNBMB4XDTEyMTAyMzEzNDEzMVoXDTE3MTAyMzEzNTEzMVow
				SzETMBEGCgmSJomT8ixkARkWA2NvbTEfMB0GCgmSJomT8ixkARkW
				D29keXNzZXlzb2Z0d2FyZTETMBEGA1UEAxMKT1NJLURFVi1DQTCC
				ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKpJQuco1yN9
				zt/6Q5texGMEQQ7mOzLw5wpIlB0/MT3LReMnBxjnG1FDSQtb4qeK
				gbpRUvBkN9HU92uvq4+T8fENcjlg8f3xUfYbgcAIbvpWelC8OdZx
				wQNpmSiUGdz6agGFRnAlPkAR0vrgVsLwsWm0xjRyeRobLT9eF7l5
				FJrbWmR8Lo5Tmxr5BeS63Fm+Qetsh//9eQDY7n19hfn7Sri9baoh
				f2foLTW/Nmz8UpEj4ilCIupMfidWNf6iaZTL936GfZcY333fJZ0f
				tIVjgDcBO+NhkudlbmlZn7WdVxFZzs69P4+LMFRIpovR1h+bl02y
				d6NF8TCBP6EKE1J3QyUCAwEAAaNmMGQwEwYJKwYBBAGCNxQCBAYe
				BABDAEEwCwYDVR0PBAQDAgGGMA8GA1UdEwEB/wQFMAMBAf8wHQYD
				VR0OBBYEFDPLJNFmQAzox2XdvxQAUMnYVOOJMBAGCSsGAQQBgjcV
				AQQDAgEAMA0GCSqGSIb3DQEBBQUAA4IBAQCCebDMEDUxr2ZmkwPT
				BmBLbVvR5DQJXv1CRoEJIY6CsEVnjERpksoE9hRaTVLguWEbD0cn
				cKYeKr+AQl8/rvGlXGcxOEiSTYSDRhoCZ7ATDSGIwFzfEgbHkKbA
				i0TQIMKIJFf7qrFczg75tnd1CT9rhwShBIxgj2XarjoUg94AvMD3
				doupzhST9MW9dHBNSsd9quoQdrFuxVftcDsDw/wOW6hwsC7tRr7K
				2ObPcAHB8i5yUO1jOjYB6rwWlBvHDyIh1ioMPiSjmcU0iyV9wp3A
				6T4PyMyW/CVnEysTPeJZWU2sF0GbRRnUI2QwC1EcqPqkVHOcwECz
				OF/qLOumb+rn
				</data>
				<key>IsIdentity</key>
				<false/>
			</dict>
			<dict>
				<key>Data</key>
				<data>
				MIICPDCCAaUCEHC65B0Q2Sk0tjjKewPMur8wDQYJKoZIhvcNAQEC
				BQAwXzELMAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJ
				bmMuMTcwNQYDVQQLEy5DbGFzcyAzIFB1YmxpYyBQcmltYXJ5IENl
				cnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTk2MDEyOTAwMDAwMFoX
				DTI4MDgwMTIzNTk1OVowXzELMAkGA1UEBhMCVVMxFzAVBgNVBAoT
				DlZlcmlTaWduLCBJbmMuMTcwNQYDVQQLEy5DbGFzcyAzIFB1Ymxp
				YyBQcmltYXJ5IENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIGfMA0G
				CSqGSIb3DQEBAQUAA4GNADCBiQKBgQDJXFme8huKARS0EN8EQNvj
				V69qRUCPhAwL0TPZ2RHP7gJYHyX3KqhEBarsAx94f56TuZoAqiN9
				1qyFomNFx3InzPRMxnVx0jnvT0Lwdd8KkMaOIG+YD/isI19wKTak
				yYbnsZogy1Olhec9vn2a/iRFM9x2Fe0PonFkTGUugWhFpwIDAQAB
				MA0GCSqGSIb3DQEBAgUAA4GBALtMEivPLCYATxQT3ab7/AoRhIzz
				KBxnki98tsX63/Dolbwdj2wsqFHMc9ikwFPwTtYmwHYBV4GSXiHx
				0bH/59AhWM1pF+NEHJwZRDmJXNycAA9WjQKZ7aKQRUzkuxCkPfAy
				Aw7xzvjoyVGM5mKf5p/AfbdynMk2OmufTqj/ZA1k
				</data>
				<key>IsIdentity</key>
				<false/>
			</dict>
			<dict>
				<key>CommonName</key>
				<string>b3a5085ba49e51867d59db2d737ef3ad6b0e3bc8:b7f07085727c5fd6f91d8c70e1feab8dc27a5e21d272e41a6dfe60e7f28c5cfb</string>
				<key>Data</key>
				<data>
				MIIDrjCCApagAwIBAgIHPGujEwAABTANBgkqhkiG9w0BAQQFADA6
				MQswCQYDVQQGEwJVUzEWMBQGA1UEChMNU3ltYW50ZWMgQ29ycDET
				MBEGA1UEAxMKQXBwIENlbnRlcjAeFw0xMzExMDQyMjQ4MDNaFw0x
				ODExMDUyMjQ4MDNaMIGZMQswCQYDVQQGEwJVUzEWMBQGA1UEChMN
				U3ltYW50ZWMgQ29ycDFyMHAGA1UEAxNpYjNhNTA4NWJhNDllNTE4
				NjdkNTlkYjJkNzM3ZWYzYWQ2YjBlM2JjODpiN2YwNzA4NTcyN2M1
				ZmQ2ZjkxZDhjNzBlMWZlYWI4ZGMyN2E1ZTIxZDI3MmU0MWE2ZGZl
				NjBlN2YyOGM1Y2ZiMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKB
				gQDM1jOXytzWso/rBjNZY7L+7y2SwFE7tLyI4Sgqfz00KvoF3XRe
				zyMBLYxrLx4djnng+EaG3oW7nGOx4Xl3uWiUrCeXy+74ArLTsHaE
				d8Mo3IVeOhgauCRat6yyCrW5hbJOFbhKHMVQW3GqGRRTlix50GGE
				oZsCpFvz+5PxD8FNhQIDAQABo4HcMIHZMAkGA1UdEwQCMAAwNAYJ
				YIZIAYb4QgENBCcWJVN5bWFudGVjIEFwcENlbnRlciBEZXZpY2Ug
				Q2VydGlmaWNhdGUwHQYDVR0OBBYEFHIDctZ8oyvTAf5aQXZiQmkY
				jo70MGoGA1UdIwRjMGGAFElPhsioa7rPfLfOpH0byaiA+xU0oT6k
				PDA6MQswCQYDVQQGEwJVUzEWMBQGA1UEChMNU3ltYW50ZWMgQ29y
				cDETMBEGA1UEAxMKQXBwIENlbnRlcoIJAJqMm2tWz59gMAsGA1Ud
				DwQEAwIF4DANBgkqhkiG9w0BAQQFAAOCAQEArCWW6QHW/u3KX9qe
				bnIhQtFPw8ftUAbhfyi6eqJf4m7DSUoY5a/4tehy2g7YdL/iXcbz
				6UGASM27SUwZx46QTZhLegrNc2Kq4DOM/xgICRxwgvJ7dSEk3ZY8
				uqBM0e/e5qI4FhdfCT+k0b3fp1L5X445JhYdCtO/x1zwWGn1r6sI
				oM5e6jghH44AeZ5+DuzaBtAWEeze5aafjBSOWIFxgb3Pivyzz9ol
				MWbaQqreFOhuGplV67lVhraPw6b4zX86BH8MZuCpex7h9gYHTZn9
				W+GWOtuOks4I802sfaE1OXdybp3bLqkcqxcgwR1XiD/nL9UfTcqt
				MmRNYcQP8dL29w==
				</data>
				<key>IsIdentity</key>
				<true/>
			</dict>
		</array>
		<key>CommandUUID</key>
		<string>$mdm_commandid</string>
		<key>Status</key>
		<string>Acknowledged</string>
		<key>UDID</key>
		<string>$deviceid</string>
	</dict>
	</plist>"
	echo $mdm_certificatelistrequest > mdm_certificatelist.request	
	mdmrequest certificatelist
fi
if [ "$mdm_requesttype" = "ClearPasscode" ]
	then
	mdm_clearpasscoderequest="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
	<plist version=\"1.0\">
	<dict>
		<key>CommandUUID</key>
		<string>$mdm_commandid</string>
		<key>Status</key>
		<string>Acknowledged</string>
		<key>UDID</key>
		<string>$deviceid</string>
	</dict>
	</plist>"
	echo $mdm_clearpasscoderequest > mdm_clearpasscode.request	
	mdmrequest clearpasscode
fi
if [ "$mdm_requesttype" = "DeviceInformation" ]
	then
	devicename=`cat ./DeviceInfo.txt | grep 'DeviceName' -A1 | grep '<string>' | sed 's/^.*<string>//;s/<\/string>.*$//'`
	mdm_deviceinformationrequest="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
	<plist version=\"1.0\">
	<dict>
		<key>CommandUUID</key>
		<string>$mdm_commandid</string>
		<key>QueryResponses</key>
		<dict>
			<key>AvailableDeviceCapacity</key>
			<real>4.6557693481445312</real>
			<key>BatteryLevel</key>
			<real>1</real>
			<key>BluetoothMAC</key>
			<string>bc:67:78:1a:29:50</string>
			<key>BuildVersion</key>
			<string>10B329</string>
			<key>CellularTechnology</key>
			<integer>0</integer>
			<key>DeviceCapacity</key>
			<real>6.3885040283203125</real>
			<key>DeviceName</key>
			<string>$devicename</string>
			<key>IsSupervised</key>
			<false/>
			<key>Model</key>
			<string>MC540LL</string>
			<key>ModelName</key>
			<string>iPod</string>
			<key>OSVersion</key>
			<string>6.1.3</string>
			<key>ProductName</key>
			<string>iPod4,1</string>
			<key>SerialNumber</key>
			<string>CCQJ51BGDT75</string>
			<key>UDID</key>
			<string>$deviceid</string>
			<key>WiFiMAC</key>
			<string>bc:67:78:1d:06:d6</string>
		</dict>
		<key>Status</key>
		<string>Acknowledged</string>
		<key>UDID</key>
		<string>$deviceid</string>
	</dict>
	</plist>"
	echo $mdm_deviceinformationrequest > mdm_deviceinformation.request	
	mdmrequest deviceinformation
fi
if [ "$mdm_requesttype" = "DeviceLock" ]
	then
	mdm_devicelockrequest="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
	<plist version=\"1.0\">
	<dict>
		<key>CommandUUID</key>
		<string>$mdm_commandid</string>
		<key>Status</key>
		<string>Acknowledged</string>
		<key>UDID</key>
		<string>$deviceid</string>
	</dict>
	</plist>"
	echo $mdm_devicelockrequest > mdm_devicelock.request	
	mdmrequest devicelock
fi
if [ "$mdm_requesttype" = "InstallApplication" ]
	then
#	<key>bundle-identifier</key><string>com.nukona.2013demo1</string>	
#	mdm_appidentifier=`cat ./mdm_command.response | grep RequestType | sed 's/^.*<key>bundle-identifier<\/key><string>//;s/<\/string>.*$//'`
	mdm_manifest=`cat ./mdm_command.response | grep RequestType | sed 's/^.*<key>ManifestURL<\/key><string>//;s/<\/string>.*$//'`
	curl -s -k -i --raw -o mdm_manifest.response "$mdm_manifest" -H "Host: $pservername" -H "Proxy-Connection: keep-alive" -H "Accept-Encoding: gzip, deflate" -H "Accept: */*" -H "Cookie: csrftoken=VQBCLVlTnqK3WzpNT0DVPyiUrczmUwF1" -H "Connection: keep-alive" -H "Accept-Language: en-us" -H "User-Agent: mdmd/1.0 CFNetwork/672.0.8 Darwin/14.0.0"
	mdm_softwarepkg=`cat ./mdm_manifest.response | grep app.ipa | sed 's/^.*<string>//;s/<\/string>.*$//'`
	curl -s -k -i --raw -o mdm_app.response -I "$mdm_softwarepkg" -H "Host: $pservername" -H "Proxy-Connection: keep-alive" -H "Accept-Encoding: gzip, deflate" -H "Accept: */*" -H "Cookie: AWSELB=1951D13B1AF976C066769F8B78C88302DE334D18DDA7B3D22CE333924DBC3399CC94D7CB861CFCE12E9AF079169B92383270E50DA9613C4A28FF97D5D9318E49C50551BD146331ECFE80655C66F928BAEA4DDF2C19; device-hash=$deviceid" -H "Accept-Language: en" -H "Connection: keep-alive" -H "User-Agent: itunesstored/1.0 iOS/7.0.4 model/iPhone3,3 (5; dt:70)"
	mdm_installapprequest="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
	<plist version=\"1.0\">
	<dict>
		<key>CommandUUID</key>
		<string>$mdm_commandid</string>
		<key>Identifier</key>
		<string>com.nukona.2013demo1</string>
		<key>State</key>
		<string>Queued</string>
		<key>Status</key>
		<string>Acknowledged</string>
		<key>UDID</key>
		<string>$deviceid</string>
	</dict>
	</plist>"
	# Request https://load.nuk9.com/appstore/api6/b426a916ea9d47665881988d49c92a1d828fc5b1/5/6/install_status.json 
		# Repeat until response "P"
		curl -s -k -i --raw -o 0.dat "https://load.nuk9.com/appstore/api6/b426a916ea9d47665881988d49c92a1d828fc5b1/5/6/install_status.json" -H "Host: $pservername" -H "Proxy-Connection: keep-alive" -H "Accept-Encoding: gzip" -H "User-Agent: Load ADA [4.20] rv:20.4 (iPhone; iPhone OS 7.0.4; en_US)" -H "Connection: keep-alive" -H "Cookie: AWSELB=1951D13B1AF976C066769F8B78C88302DE334D18DD67EF46F772B00D5670A8B7E6B3ED65A1F8B3334853FF4D9D6727D9974F98A4E2613C4A28FF97D5D9318E49C50551BD146331ECFE80655C66F928BAEA4DDF2C19; csrftoken=I3TT8BXbOJmhEE8xIvJN5fCAbxSp51d5; device-hash=$deviceid"
		echo $mdm_installapprequest > mdm_installapp.request	
		mdmrequest installapp
fi
if [ "$mdm_requesttype" = "InstalledApplicationList" ]
	then
	mdm_installedapplicationlistrequest="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
	<plist version=\"1.0\">
	<dict>
		<key>CommandUUID</key>
		<string>$mdm_commandid</string>
		<key>InstalledApplicationList</key>
		<array>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer</string>
				<key>Name</key>
				<string>Load ADA</string>
				<key>ShortVersion</key>
				<string>[4.20]</string>
				<key>Version</key>
				<string>20.4</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer02</string>
				<key>Name</key>
				<string>Load ADA02</string>
				<key>ShortVersion</key>
				<string>[4.2002]</string>
				<key>Version</key>
				<string>20.402</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer03</string>
				<key>Name</key>
				<string>Load ADA03</string>
				<key>ShortVersion</key>
				<string>[4.2003]</string>
				<key>Version</key>
				<string>20.403</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer04</string>
				<key>Name</key>
				<string>Load ADA04</string>
				<key>ShortVersion</key>
				<string>[4.2004]</string>
				<key>Version</key>
				<string>20.404</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer05</string>
				<key>Name</key>
				<string>Load ADA05</string>
				<key>ShortVersion</key>
				<string>[4.2005]</string>
				<key>Version</key>
				<string>20.405</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer06</string>
				<key>Name</key>
				<string>Load ADA06</string>
				<key>ShortVersion</key>
				<string>[4.2006]</string>
				<key>Version</key>
				<string>20.406</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer07</string>
				<key>Name</key>
				<string>Load ADA07</string>
				<key>ShortVersion</key>
				<string>[4.2007]</string>
				<key>Version</key>
				<string>20.407</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer08</string>
				<key>Name</key>
				<string>Load ADA08</string>
				<key>ShortVersion</key>
				<string>[4.2008]</string>
				<key>Version</key>
				<string>20.408</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer09</string>
				<key>Name</key>
				<string>Load ADA09</string>
				<key>ShortVersion</key>
				<string>[4.2009]</string>
				<key>Version</key>
				<string>20.409</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer10</string>
				<key>Name</key>
				<string>Load ADA10</string>
				<key>ShortVersion</key>
				<string>[4.2010]</string>
				<key>Version</key>
				<string>20.410</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer11</string>
				<key>Name</key>
				<string>Load ADA11</string>
				<key>ShortVersion</key>
				<string>[4.2011]</string>
				<key>Version</key>
				<string>20.411</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer12</string>
				<key>Name</key>
				<string>Load ADA12</string>
				<key>ShortVersion</key>
				<string>[4.2012]</string>
				<key>Version</key>
				<string>20.412</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer13</string>
				<key>Name</key>
				<string>Load ADA13</string>
				<key>ShortVersion</key>
				<string>[4.2013]</string>
				<key>Version</key>
				<string>20.413</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer14</string>
				<key>Name</key>
				<string>Load ADA14</string>
				<key>ShortVersion</key>
				<string>[4.2014]</string>
				<key>Version</key>
				<string>20.414</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer15</string>
				<key>Name</key>
				<string>Load ADA15</string>
				<key>ShortVersion</key>
				<string>[4.2015]</string>
				<key>Version</key>
				<string>20.415</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer16</string>
				<key>Name</key>
				<string>Load ADA16</string>
				<key>ShortVersion</key>
				<string>[4.2016]</string>
				<key>Version</key>
				<string>20.416</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer17</string>
				<key>Name</key>
				<string>Load ADA17</string>
				<key>ShortVersion</key>
				<string>[4.2017]</string>
				<key>Version</key>
				<string>20.417</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer18</string>
				<key>Name</key>
				<string>Load ADA18</string>
				<key>ShortVersion</key>
				<string>[4.2018]</string>
				<key>Version</key>
				<string>20.418</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer19</string>
				<key>Name</key>
				<string>Load ADA19</string>
				<key>ShortVersion</key>
				<string>[4.2019]</string>
				<key>Version</key>
				<string>20.419</string>
			</dict>
			<dict>
				<key>BundleSize</key>
				<integer>23158784</integer>
				<key>DynamicSize</key>
				<integer>999424</integer>
				<key>Identifier</key>
				<string>com.nuk9.load.installer20</string>
				<key>Name</key>
				<string>Load ADA20</string>
				<key>ShortVersion</key>
				<string>[4.2020]</string>
				<key>Version</key>
				<string>20.420</string>
			</dict>			
		</array>
		<key>Status</key>
		<string>Acknowledged</string>
		<key>UDID</key>
		<string>$deviceid</string>
	</dict>
	</plist>"
	echo $mdm_installedapplicationlistrequest > mdm_installedapplicationlist.request
	mdmrequest installedapplicationlist
fi
if [ "$mdm_requesttype" = "InstallProfile" ]
	then
	mdm_installprofilerequest="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
	<plist version=\"1.0\">
	<dict>
		<key>CommandUUID</key>
		<string>$mdm_commandid</string>
		<key>Status</key>
		<string>Acknowledged</string>
		<key>UDID</key>
		<string>$deviceid</string>
	</dict>
	</plist>"
	echo $mdm_installprofilerequest > mdm_installprofile.request	
	mdmrequest installprofile
fi
if [ "$mdm_requesttype" = "ManagedApplicationList" ]
	then
	# INSERT THIS INSIDE <key>ManagedApplicationList</key> for each application that is installed on device
	# <dict>
	# 	<key>com.samuelgoshen.dominodogwildforest</key>
	# 	<dict>
	# 		<key>HasConfiguration</key>
	# 		<true/>
	# 		<key>HasFeedback</key>
	# 		<false/>
	# 		<key>ManagementFlags</key>
	# 		<integer>1</integer>
	# 		<key>Status</key>
	# 		<string>Prompting</string>
	# 	</dict>
	# </dict>	
	mdm_managedapplistrequest="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
	<plist version=\"1.0\">
	<dict>
		<key>CommandUUID</key>
		<string>$mdm_commandid</string>
		<key>ManagedApplicationList</key>
		<key>Status</key>
		<string>Acknowledged</string>
		<key>UDID</key>
		<string>$deviceid</string>
	</dict>
	</plist>"
	echo $mdm_managedapplistrequest > mdm_manapplist.request	
	mdmrequest manapplist
fi
if [ "$mdm_requesttype" = "ProfileList" ]
	then
	mdm_profilelistrequest="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
	<plist version=\"1.0\">
	<dict>
		<key>CommandUUID</key>
		<string>$mdm_commandid</string>
		<key>ProfileList</key>
		<array>
			<dict>
				<key>HasRemovalPasscode</key>
				<false/>
				<key>IsEncrypted</key>
				<false/>
				<key>PayloadContent</key>
				<array>
					<dict>
						<key>PayloadDisplayName</key>
						<string>DO_NOT_TRUST_FiddlerRoot</string>
						<key>PayloadIdentifier</key>
						<string>8a99bba4a47537f07d217c0f976dd6057dd13fffa</string>
						<key>PayloadType</key>
						<string>com.apple.security.pkcs1</string>
						<key>PayloadVersion</key>
						<integer>1</integer>
					</dict>
				</array>
				<key>PayloadDisplayName</key>
				<string>DO_NOT_TRUST_FiddlerRoot</string>
				<key>PayloadIdentifier</key>
				<string>8a99bba4a47537f07d217c0f976dd6057dd13fffa</string>
				<key>PayloadRemovalDisallowed</key>
				<false/>
				<key>PayloadUUID</key>
				<string>2D7BE4E0-AEEC-41E9-94C7-3323F7B606D3</string>
				<key>PayloadVersion</key>
				<integer>1</integer>
			</dict>
		</array>
		<key>Status</key>
		<string>Acknowledged</string>
		<key>UDID</key>
		<string>$deviceid</string>
	</dict>
	</plist>"
	echo $mdm_profilelistrequest > mdm_profilelist.request	
	mdmrequest profilelist
fi
if [ "$mdm_requesttype" = "RemoveProfile" ]
	then
	mdm_removeprofilerequest="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
	<plist version=\"1.0\">
	<dict>
		<key>CommandUUID</key>
		<string>$mdm_commandid</string>
		<key>Status</key>
		<string>Acknowledged</string>
		<key>UDID</key>
		<string>$deviceid</string>
	</dict>
	</plist>"
	echo $mdm_removeprofilerequest > mdm_removeprofile.request	
	mdmrequest removeprofile
fi
if [ "$mdm_requesttype" = "Restrictions" ]
	then
	mdm_restrictionsrequest="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
	<plist version=\"1.0\">
	<dict>
		<key>CommandUUID</key>
		<string>$mdm_commandid</string>
		<key>GlobalRestrictions</key>
		<dict>
			<key>restrictedBool</key>
			<dict>
				<key>forceEncryptedBackup</key>
				<dict>
					<key>value</key>
					<true/>
				</dict>
			</dict>
			<key>restrictedValue</key>
			<dict/>
		</dict>
		<key>ProfileRestrictions</key>
		<dict>
			<key>8a99bba4a47537f07d217c0f976dd6057dd13fffa</key>
			<dict>
				<key>restrictedBool</key>
				<dict/>
				<key>restrictedValue</key>
				<dict/>
			</dict>
			<key>com.profile.symc.enroll</key>
			<dict>
				<key>restrictedBool</key>
				<dict>
					<key>forceEncryptedBackup</key>
					<dict>
						<key>value</key>
						<true/>
					</dict>
				</dict>
				<key>restrictedValue</key>
				<dict/>
			</dict>
		</dict>
		<key>Status</key>
		<string>Acknowledged</string>
		<key>UDID</key>
		<string>$deviceid</string>
	</dict>
	</plist>"
	echo $mdm_restrictionsrequest > mdm_restrictions.request	
	mdmrequest restrictions
fi
if [ "$mdm_requesttype" = "SecurityInfo" ]
	then
	mdm_securityinforequest="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
	<plist version=\"1.0\">
	<dict>
		<key>CommandUUID</key>
		<string>$mdm_commandid</string>
		<key>SecurityInfo</key>
		<dict>
			<key>HardwareEncryptionCaps</key>
			<integer>3</integer>
			<key>PasscodeCompliant</key>
			<true/>
			<key>PasscodeCompliantWithProfiles</key>
			<true/>
			<key>PasscodePresent</key>
			<true/>
		</dict>
		<key>Status</key>
		<string>Acknowledged</string>
		<key>UDID</key>
		<string>$deviceid</string>
	</dict>
	</plist>"
	echo $mdm_securityinforequest > mdm_securityinfo.request
	mdmrequest securityinfo
fi
}
# Switch comment to next line for production
deviceid=`cat ./DeviceInfo.txt | grep 'UDID' -A1 | grep '<string>' | sed 's/^.*<string>//;s/<\/string>.*$//'`
core_device_id=`cat ./DeviceInfo.txt | grep 'CoreDeviceID' -A1 | grep '<string>' | sed 's/^.*<string>//;s/<\/string.*$//'`
#deviceid="c500cf3962b26495818567ce738a1d0000000001"
echo "Device ID:"$deviceid
echo "Core Device ID:"$core_device_id
echo "Status Idle Request"
mdm_statusidlerequest="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
	<plist version=\"1.0\">
	<dict>
		<key>Status</key>
		<string>Idle</string>
		<key>UDID</key>
		<string>$deviceid</string>
	</dict>
	</plist>"
echo $mdm_statusidlerequest > mdm_statusidle.request
mdmrequest statusidle
if [ "$serverversion" = "4.1" ]
then
	mdm_requesttype=`cat ./mdm_command.response | grep RequestType -A1 | grep '<string>' | sed 's/^.*<string>//;s/<\/string>.*$//'`
	mdm_commandid=`cat ./mdm_command.response | grep CommandUUID -A1 | grep '<string>' | sed 's/^.*<string>//;s/<\/string>.*$//'`	
else # assume 4.2 as default
	mdm_requesttype=`cat ./mdm_command.response | grep RequestType | sed 's/^.*RequestType<\/key><string>//;s/<\/string>.*$//'`
	mdm_commandid=`cat ./mdm_command.response | grep CommandUUID | sed 's/^.*CommandUUID<\/key><string>//;s/<\/string>.*$//'`
fi
mdmapprandom=$(( ( RANDOM % 10 )  + 1 ))
if [ "$mdmapprandom" == 1 ]
	then
	echo "MDM Install Application"
	# Login
curl -s -k -i -D cookiemonster.txt --raw -o 0Login.dat --data "username=user000001&password=Symantec2@" "https://load.nuk9.com/appstore/api6/login"	
sessionid=`cat ./0Login.dat | grep sessionid | sed 's/^.*Set-Cookie: sessionid=//;s/; httponly.*$//'`
	# Request Details.json
curl -s -k -i -b cookiemonster.txt --raw -o 1Details.dat "https://load.nuk9.com/appstore/api6/app/$core_device_id/apps/5/details.json" -H "Host: $servername" -H "Proxy-Connection: keep-alive" -H "Accept-Encoding: gzip" -H "Accept-Language: en" -H "Connection: keep-alive" -H "User-Agent: Load ADA [4.20] rv:20.4 (iPhone; iPhone OS 7.0.4; en_US)"
	# Request App Install
curl -s -k -i -b cookiemonster.txt --raw -o 2InstallApp.dat "https://load.nuk9.com/appstore/mdmapi2/$core_device_id/6/install_app" -H "Host: $servername" -H "Proxy-Connection: keep-alive" -H "Accept-Encoding: gzip" -H "Accept-Language: en" -H "Connection: keep-alive" -H "User-Agent: Load ADA [4.20] rv:20.4 (iPhone; iPhone OS 7.0.4; en_US)"	
	#mdmrequest installapplication
fi
while [[ -n "$mdm_requesttype" ]]; do
	if [ "$serverversion" = "4.1" ]
	then
		mdm_requesttype=`cat ./mdm_command.response | grep RequestType -A1 | grep '<string>' | sed 's/^.*<string>//;s/<\/string>.*$//'`
		mdm_commandid=`cat ./mdm_command.response | grep CommandUUID -A1 | grep '<string>' | sed 's/^.*<string>//;s/<\/string>.*$//'`
	else
		mdm_requesttype=`cat ./mdm_command.response | grep RequestType | sed 's/^.*RequestType<\/key><string>//;s/<\/string>.*$//'`
		mdm_commandid=`cat ./mdm_command.response | grep CommandUUID | sed 's/^.*CommandUUID<\/key><string>//;s/<\/string>.*$//'`
	fi
	    echo $mdm_requesttype $mdm_commandid	
            processcommands $mdm_commandid $deviceid $mdm_requesttype $servername $core_device_id
done
ELAPSED_TIME=$(($SECONDS - $START_TIME))
  echo "Elapsed Time:" $ELAPSED_TIME
exit
