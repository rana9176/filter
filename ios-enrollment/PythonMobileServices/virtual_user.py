import httplib, urllib, json, time, sys, os
import settings
import requests


class VirtualUser():
    def __init__(self, id):
        self.id = id  # Identifies the user
        if settings.LEADING_ZEROS == 1:
            self.username = settings.DEVICE + str(id).zfill(7)
        else:
            self.username = settings.DEVICE + str(id)
        self.password = "Symantec1"
        self.first_name = self.username
        self.last_name = "Test"
        self.email = self.username + settings.EMAIL_SUFFIX
        newkey = {'api_key': settings.APIKEY}
        self.encoded_key = urllib.urlencode(newkey)
        if not os.path.exists(settings.FILE_PATH + self.username):
            os.makedirs(settings.FILE_PATH + self.username)

    def __unicode__(self):
        return unicode('id = %s' % (self.id))

    @property
    def username(self):
        if settings.LEADING_ZEROS == 1:
            return settings.USER_PREFIX + str(self.id).zfill(7)
        else:
            return settings.USER_PREFIX + str(self.id)


    @property
    def password(self):
        return "Symantec1"
        # return ('0401%s' % str(self.id).zfill(36))

    @property
    def first_name(self):
        return self.username

    @property
    def last_name(self):
        return "Test"

    @property
    def email(self):
        return self.username + settings.EMAIL_SUFFIX

    def add_user_to_AC(self, usern):
        # params = urllib.urlencode({'username': settings.ADMIN_USERNAME, 'password': settings.ADMIN_PASSWORD})
        # Check if user exists
        conn = httplib.HTTPSConnection(settings.HOSTNAME)
        if (self.user_exists(conn, usern) == 1):  # true
            return;
        user_params = {'username': usern}
        user_params.update({'password': 'Symantec1'})
        user_params.update({'first_name': usern})
        user_params.update({'last_name': 'Test'})
        user_params.update({'email': usern + settings.EMAIL_SUFFIX})
        # user_params = json.dumps(user_params)
        # data = '{"user"=%s' % (user_params)+',"api_key"="%s"}' %(settings.APIKEY)
        #data = '{"api_key":"%s"}' %(settings.APIKEY)
        print user_params
        data = {"user": json.dumps(user_params), "api_key": "%s" % (settings.APIKEY)}
        print data
        headers = {'Content-type': 'application/x-www-form-urlencoded'}
        #       data = 'user={"username": "'+ add_username +'", "password": "Symantec1", "first_name": "'+ add_username +'", "last_name": "Test", "email": "'+ this_email +'"}'
        print "https://" + settings.HOSTNAME + "/api1/users/add"
        r = requests.post("https://" + settings.HOSTNAME + "/api1/users/add", data=data, headers=headers)
        print r.json
        if r.status_code == 200:
            print('Created %s' % usern)
        else:
            print "Error while creating user",r.json
            #return user_obj;

    def user_exists(self, conn, usern):
        conn.request("GET", "/api1/users/" + usern + "?" + self.encoded_key)
        r3 = conn.getresponse()
        data = r3.read()
        user_info = json.loads(data)
        if (user_info['status'] == 'error'):
            return 0  # false
        else:
            return 1  # true