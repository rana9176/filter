#
# All Rights Reserved.
# Copyright 2013 Symantec Corporation
#
from __future__ import absolute_import
import cookielib
import urllib2

from logging2 import log
import settings

#Cookie handler so we dont have to worry about checking, storing, sending ourselves.  These will
#be stored in memory only and not on the filesystem.
cj = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))

class RequestWithMethod(urllib2.Request):
    def __init__(self, method, *args, **kwargs):
    # This assignment works directly in older Python versions
        self._method = method
        urllib2.Request.__init__(self, *args, **kwargs)

    def get_method(self):
        if self._method:
            return self._method
        elif self.has_data():
            return 'POST'
        else:
            return 'GET'

#Generates and returns the base url used for requests.
def baseUrl():
    return "https://%s:%s/" % (settings.HOSTNAME, settings.PORT)


#Makes a request to the url using the data supplied and returns the response.
def makeRequest(url, data, method=None, headers=None):
    log("Making request '%s'" % url)
    #if data and data is str and data != "":
        #log("Data: " + data)
        #log("Posting data", data)

    if method:
        print 'Method = %s' % method
        request = RequestWithMethod(method, url, data)
    else:

        request = urllib2.Request(url, data)
    if headers:
        for key in headers.keys():
            request.add_unredirected_header(key, headers[key])
            #request.add_header(key, headers[key])

    r = opener.open(request)
    return r


def makeRequest1(url, data, method=None, headers=None):
    log("Making request '%s'" % url)
    #if data and data is str and data != "":
        #log("Data: " + data)
        #log("Posting data", data)

    if method:
        print 'Method = %s' % method
        request = RequestWithMethod(method, url, data)
    else:

        request = urllib2.Request(url, data)
    if headers:
        for key in headers.keys():
            request.add_unredirected_header(key, headers[key])
            request.add_header(key, headers[key])

    r = opener.open(request)
    return r
