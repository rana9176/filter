#
# All Rights Reserved.
# Copyright 2013 Symantec Corporation
#

import MySQLdb

class MdmCoreDb():
    def __init__(self, user, passwd):
        self.user = user
        self.passwd = passwd
        self.connection = None

    def __open(self):
        self.connection = MySQLdb.connect(user=self.user, passwd=self.passwd, db='mdmcore_acload')

    def __commit_and_close(self):
        self.connection.commit()
        self.connection.close()

    def execute(self, command):
        try:
            self.__open()
            cursor = self.connection.cursor()
            cursor.execute(command)
            cursor.close()
            self.__commit_and_close()
        except Exception as e:
            log("Error occurred executing db command '%s': %s" % (command, e.message))


    def add_to_db(self):
        command = "Insert Into DeviceIos VALUES ('{0}', '2013-10-24 00:00:00', '{1}', NULL, 1, 1, 'UemAUZcmSMIMtKXpenZ1oiGFqA8/4OhADAHyDTig5tI=', 'REFUQQAABORWRVJTAAAABAAAAARUWVBFAAAABAAAAAJVVUlEAAAAEHK5De4O9EjhiUplRHM42z1ITUNLAAAAKP3nl8CobJARedwkFCiWVgBX4ecR1qgbuvV0yShEqUlPCY/Rg+HvDBlXUkFQAAAABAAAAAFTQUxUAAAAFBAdwXOMb1+LbkWxwDhZxsXEDsXuSVRFUgAAAAQAAMNQVVVJRAAAABCMeAsdP6JCqaZmAy+NYBgDQ0xBUwAAAAQAAAABV1JBUAAAAAQAAAADS1RZUAAAAAQAAAAAV1BLWQAAACgZMUyZmFmJ3L6U0VzmgtY0EODHh7l0KDT/jOWy5NRl0J3na/6XLRpiVVVJRAAAABCO0V97QcdOF68Xs4LKCjLGQ0xBUwAAAAQAAAACV1JBUAAAAAQAAAADS1RZUAAAAAQAAAABV1BLWQAAACilKUDhjw6hKtnd3/VnvyeELuW6ktbpqkw36MNYUFIF9J+6HQsApoJ1UEJLWQAAACA7j2YNl/EIQf1Ae65IRwsej0bGPTRmkCMWLHQimYVBUlVVSUQAAAAQYINLtkPuR3OIEm3bZ5KKJ0NMQVMAAAAEAAAAA1dSQVAAAAAEAAAAA0tUWVAAAAAEAAAAAFdQS1kAAAAo4eeip9M6YRn8+neaLf7pohzbAcjD+ekeuvjAetYHzz/ajSvlV808RlVVSUQAAAAQhYJ9Ov8qTwCT816WuMc3Z0NMQVMAAAAEAAAABVdSQVAAAAAEAAAAA0tUWVAAAAAEAAAAAFdQS1kAAAAoD3fYap1asC63qL4/+Un7mO8NAFtYf+6F4O93E+g9NrKwGf456iFg31VVSUQAAAAQjWurNROoRQ286QDXhVSHiUNMQVMAAAAEAAAABldSQVAAAAAEAAAAA0tUWVAAAAAEAAAAAFdQS1kAAAAoP1DTO803jXWp4mRaLkgpi4TuIKYkRh6jEIz92cvCbcu6Q+HFt+bRplVVSUQAAAAQ071U2PQ0T8y+yCY3FXcblkNMQVMAAAAEAAAAB1dSQVAAAAAEAAAAA0tUWVAAAAAEAAAAAFdQS1kAAAAoaTK2d6gnWdbtMpQpS8A1dzYPlKaoCLoT+td0vkJt6feU8pX8qL0+QlVVSUQAAAAQzNzoILv5R9i99dg8yinFcUNMQVMAAAAEAAAACFdSQVAAAAAEAAAAAUtUWVAAAAAEAAAAAFdQS1kAAAAg0z10VOUJxwDH3cFtzfNpXeZbyVPOxxGRXs293ia3vslVVUlEAAAAEFaXPF89tEwOvNCyRlpIGSlDTEFTAAAABAAAAAlXUkFQAAAABAAAAANLVFlQAAAABAAAAABXUEtZAAAAKJ+Bjpn9cNIpXCo44EQn7E9jjAoidMY42Q4V4ZVFP4iqRMLkqo6eu4ZVVUlEAAAAEDvvlRjb10wQm3Ni/md9ospDTEFTAAAABAAAAApXUkFQAAAABAAAAANLVFlQAAAABAAAAABXUEtZAAAAKD+MDo1gQrQCOJjJuWYTRHSw2e28/VpnOGetDhg8V79thx+u7ww0jb9VVUlEAAAAELzMhGJzA0iIkMDYEzTfHLRDTEFTAAAABAAAAAtXUkFQAAAABAAAAAFLVFlQAAAABAAAAABXUEtZAAAAIN3gFs+2zU61sNBCotlir1x5gz3BIxfX9WgxxT53+FgVU0lHTgAAABRpH5nnU7t7o3wm6+0Ytbqufa+rPQ==', '9B0A4DB4-B0C0-461E-9A87-A9FDEC7CF266', 'com.apple.mgmt.External.b1a5c5cc-9922-4568-96ea-12ea76ca806c', NULL, 'B472112F398FEFD2BCA8EC1C86AB9B08456E40C6', '2013-10-24 15:52:59', 'MIIDojCCAoqgAwIBAgICAJ0wDQYJKoZIhvcNAQEEBQAwOjELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDVN5bWFudGVjIENvcnAxEzARBgNVBAMTCkFwcCBDZW50ZXIwHhcNMTMxMDI0MTM1NDU1WhcNMTgxMDI1MTM1NDU1WjCBmTELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDVN5bWFudGVjIENvcnAxcjBwBgNVBAMTaWFlOTkxZmJmNzVlZmFiYjliYzU2NmQwNzY4OTRkODdlNjFiMjg5MjU6ZGE3OTQ0Y2Q5N2Y5NDZhMjQxNzYxZjEyZjU2MWM5N2FkYjI0NzBhMDYyOTdhOTE0Y2RkNjJjMzM4OThjZDU5ZTCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAyhmuOKZ1sPw3DPQ799j55BVTYVeh4NitFU3m8i4Gc1K6kYktTuK0rka6OyugIPb797OtoYwQjXwV5XaQ24AVCAvhl0Jd7WNMkJI/V7Ihc0ZMW2ADroTnDGiA9Y6889XJ5V7WU3o9ppFLha0IyNOLjgOdUhYCeNUnK0XJHrSaDFMCAwEAAaOB1TCB0jAJBgNVHRMEAjAAMC0GCWCGSAGG+EIBDQQgFh5OdWtvbmEsIEluYyBEZXZpY2UgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFO1KSrKzyBdt8IERhrMOcW9QWAjCMGoGA1UdIwRjMGGAFElPhsioa7rPfLfOpH0byaiA+xU0oT6kPDA6MQswCQYDVQQGEwJVUzEWMBQGA1UEChMNU3ltYW50ZWMgQ29ycDETMBEGA1UEAxMKQXBwIENlbnRlcoIJAJqMm2tWz59gMAsGA1UdDwQEAwIF4DANBgkqhkiG9w0BAQQFAAOCAQEAKcROK0kLoI2zFvym7seB2dOrj1iQVt3Cb/hwT3ao6EHHb12CG3HPYqtPg+Aoc5AXiCULZhdb2RZwueSSgBsWh1l8WADjSIzigMLNw+XWCLgKtTQJJZruBjcdj9tMoNniiVOvUGuDJLrXi5ZKdHvgLyF1Ypo2nfGX7FscHSf8o+n+LV0BleEbgXXS9RkCOB7bPF+96QvRK1EsI7ZRiKFiSK4RbZDEVze+tLrjKF1OD9f5TtnwDRckdWkU17f2OD1U5UF/df6v4SIAFCIxlhI95nHDqpSPXawqGAdL7dc+6qDEeRJXOlCACUkFKAE18QYQkK7GVv5H9EqEbMszBE2+YA==')"
        command = command.format(self.device_id, self.udid)
        db = MdmCoreDb(DBUSER, DBPASSWD)
        db.execute(command)

    def remove_from_db(self):
        command = "DELETE FROM DeviceIos WHERE DeviceId = '{0}'"
        command = command.format(self.device_id)
        db = MdmCoreDb(DBUSER, DBPASSWD)
        db.execute(command)

    def add_command_to_db(self):
        command = "INSERT INTO IosCommand VALUES ('{0}', '{1}', 'REALTIME', 'LOCK', '<dict><key>CommandUUID</key><string>{0}</string><key>Command</key><dict><key>RequestType</key><string>DeviceLock</string><key>PIN</key><string></string><key>Message</key><string></string><key>PhoneNumber</key><string></string></dict></dict>', 15, '2013-10-23 16:57:46', 'QUEUED', '2013-10-23 21:56:13')"
        command = command.format(uuid.uuid4(), self.udid)
        db = MdmCoreDb(DBUSER, DBPASSWD)
        db.execute(command)
