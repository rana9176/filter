from __future__ import absolute_import
from py4j.java_gateway import JavaGateway, GatewayClient

# Ensure that the port parameter used in JavaGateway is the same as the port number used in the ScepClient.java file
# (look in the main() static method
gateway = JavaGateway(GatewayClient(port=25335))
app = gateway.entry_point
ret = app.doScep('https://wrv.nuk9.com/appstore/scep', 'privateKey.pkcs8',
                 "certifcate.crt", 'CSR.csr',
                 "issuedcert.cer", r'/Users/willy_vasquezrivas/Downloads/')