'''
Created on Jun 25, 2014

@author: aswathy_Jayachandran
'''

from __future__ import absolute_import
import sys,os
import httplib, urllib, json, time
sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from device import VirtualDevice
from virtual_user import VirtualUser
from logging2 import log
from communication import baseUrl, makeRequest
import settings
import json


class MDMUtils():

    def create_user(self, user_id):
        user = VirtualUser(user_id)
        user.add_user_to_AC(user.username)
        user_details = {'name' : user.username, 'first_name' : user.first_name, 'last_name' : user.last_name, 'password' : user.password, 'email' : user.email}
        print("\nuser_details = %s"%(user_details))
        return user

    '''
    # @author Aswathy
    # @brief creates and register the IOS device with App Center
    # @return ios_device - device with properties {hash, device_id, udid, acid, device_cert, encryption_cert, identity_cert, token, magic}
    '''
    def register_ios_device(self, device_id, username, password):
        ios_device = VirtualDevice(device_id)
        ios_device.enroll(username, password)
        device_details = {'device_hash' : ios_device.hash, 'id' : ios_device.device_id, 'udid' : ios_device.udid, 'acid' : ios_device.acid, 'db_id' : ios_device.id}
        print("device_details = %s"%(device_details))
        return ios_device

    '''
    # @author Aswathy
    # @brief Enroll MDM in IOS device
    '''
    def enroll_mdm_InIOSDevice(self, ios_device):
        is_enrolled = ios_device.enroll_mdm()
        return is_enrolled

    '''
    # @author Aswathy'''
    def process_command(self, device):
        path = settings.FILE_PATH + device.get_user() + '/'
        device.mdm.process_commands(device.udid, path)



