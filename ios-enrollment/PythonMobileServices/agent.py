#
# All Rights Reserved.
# Copyright 2013 Symantec Corporation
#

import base64
import json
import subprocess
import M2Crypto

from communication import makeRequest, baseUrl
import settings

class ACAgent():
    def __init__(self, config_dict):
        self.config_dict = config_dict
        self.hash = self.config_dict['udid']

    def enroll_to_ac(self):
        if True: #Check if we are already logged in. (len(cj._cookies) == 0):
            self.login(settings.USERNAME, settings.PASSWORD)
        self.device_json()

    def login(self, username, password):
        url = baseUrl() + 'appstore/api6/{0}/login'
        url = url.format(self.hash)
        data = 'username=%s&password=%s' % (username, password)
        r = makeRequest(url, data)
        resp = r.read()
        return resp

    def device_json(self):
        url = baseUrl() + 'appstore/api6/{0}/device.json'
        url = url.format(self.hash)
        # device = {'platform': 'ios'}
        device = {}
        device.update(self.config_dict)
        # device.update({'device_class': 'phone'})
        # device.update({'icon_px': 72})
        # device.update({'sdk_version': '1'})
        # device.update({'version': '1'})
        # device.update({'is_rooted': 'false'})
        # device.update({'product_string': 'product'})
        # device.update({'version_string': '7.0'})
        # device.update({'customer_email': self.config_dict['customer_email']})
        # device.update({'phone_id': ''})
        # device.update({'mac_address': self.device.macAddress})
        # device.update({'push_registration_account': ''})
        # device.update({'push_registration_id': ''})
        # device.update({'is_mdm_enabled': 'true'})
        # device.update({'product_name': 'product name'})
        # device.update({'model_number': ''})
        # device.update({'manufacturer': 'Apple'})
        # device.update({'IMEI': ''})
        # device.update({'udid': self.device.hash})
        # device.update({'device_name': self.device.deviceName})
        # device.update({'serial_number': '123abc'})
        # device.update({'core_device_id': ''})
        # device.update({'first_time': 'true'})
        device = json.dumps(device)
        data = 'device=%s&version_code=%s' % (device, 18)
        r = makeRequest(url, data)
        resp = r.read()
        return resp

    def set_tnc(self):
        url = baseUrl() + 'appstore/api6/{0}/set_mdm_tnc_status.json'
        url = url.format(self.hash)
        data = 'tnc_status=%s&timestamp=%s&client_version=1.0' % ('true', '2013-01-01-00-00-00')
        r = makeRequest(url, data)
        resp = r.read()
        return resp

    def get_mdminstall_url(self):
        url = baseUrl() + 'appstore/mdmapi1/{0}/get_mdminstall_url.json'
        url = url.format(self.hash)
        r = makeRequest(url, None)
        resp_json = json.loads(r.read())
        return resp_json['mdm_install_url']

    def get_mdminstallsplash(self, url):
        url = url + 'webclip=1' #TODO - make webclip conditional
        r = makeRequest(url, None)
        resp = r.read()
        return resp

    def get_enroll_url(self, resp):
        start_str = 'window.location.href = '
        start = resp.find(start_str) + len(start_str) + 1
        end = resp.find(';', start) - 1
        return resp[start:end]

