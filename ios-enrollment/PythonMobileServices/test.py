#
# All Rights Reserved.
# Copyright 2013 Symantec Corporation
#
from __future__ import absolute_import
import sys
from device import VirtualDevice
from logging2 import log
import settings
import logging2

START_ID = 1
settings.customer_id = sys.argv[1]
settings.domain_id = sys.argv[2]
settings.user_id = sys.argv[3]
settings.ADMIN_USERNAME = sys.argv[4]
settings.ADMIN_PASSWORD = sys.argv[5]
settings.r3_url = sys.argv[6]
settings.scep_url = sys.argv[7]
settings.IS_MAC = sys.argv[8]
settings.retry_count = int(sys.argv[9])
END_ID = START_ID

class TestHarness():
    #constructor
    def __init__(self):
        self.devices = []

    def just_go(self):
        self.setup()
        #self.run_tests()
        self.teardown()

    def setup(self):
        # Get number of devices to test.
        #num_devices = 1#int(raw_input('Enter number of devices: '))

        # Create devices.
        log('Creating %s devices' % (END_ID - START_ID + 1))
        for i in range(START_ID, END_ID):
            self.devices.append(VirtualDevice(i))

        log('Enrolling devices to AC')
        for device in self.devices:
            device.agent.enroll_to_ac()


    def run_tests(self):
        log('Processing commands for devices')
        for device in self.devices:
            device.process_commands()

    def teardown(self):
        log('Removing devices from MDM Core DB')
        for device in self.devices:
            device.remove_from_db()

try:
    logging2.backupOldLogIFile()
    logging2.openFileForLogging()
    test_device = VirtualDevice(START_ID)
    test_device.enroll_mdm(settings.r3_url)
    print "Error message: %s" %settings.errorMessage
    if settings.errorMessage is not 'none':
        print "%s"%settings.errorMessage
except Exception as e:
    print e.message 
finally:
    logging2.closeLogFile()
