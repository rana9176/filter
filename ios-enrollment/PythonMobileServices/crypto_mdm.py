import subprocess
from logging2 import log
import settings

def sign_data(data, cert, key, path, detached=False):
    from pkcs7 import pkcs7 as p7
    from base64 import b64encode

    cert_file = '%s%s' % (path, cert)
    key_file = '%s%s' % (path, key)


    if isinstance(data, unicode):
        data = data.encode('utf8')


    try:

        p7klass = p7(cert=cert_file, key=key_file)
        p7_der = p7klass.pkcs7_sign_content(data, detached=detached)
        return b64encode(p7_der)
    except Exception as e:
        log('Error occurred signing data: %s' % e.message)
        settings.errorMessage='Error Message: Error occurred signing data: %s' % e.message
    return None

def create_certificate(path, cert_name, key_name, subject=None):
    log('Creating certificate')
    crt_file = '%s%s.crt' % (path, cert_name)
    key_file = '%s%s.key' % (path, key_name)
    log('cert file: %s' %crt_file)
    log('key file: %s' %key_file)

    cmd = [
        settings.OPENSSL_PATH,
        'req',
        '-x509',
        '-nodes',
        '-days',
        '365',
        '-newkey',
        'rsa:1024',
        '-out',
        crt_file,
        '-keyout',
        key_file
    ]
    #cmd = settings.OPENSSL_PATH
    #cmd_args = "req -x509 -nodes -days 365 -newkey rsa:1024 -out " + crt_file + " -keyout " + key_file
    if subject:
        cmd.extend(['-subj', subject])
        #cmd_args = cmd_args + " -subj " + subject
        log('command: %s' %cmd)

    try:
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=settings.ENV)
        output, error = p.communicate()
        return output
    except Exception as e:
        log('Error occurred creating certificate: %s' % e.message)
        settings.errorMessage='Error Message: Error occurred creating certificate: %s' % e.message
    return None

def create_csr(path, csr, key, subject, challenge):
    csr_file = '%s%s.csr' % (path, csr)
    key_file = '%s%s.key' % (path, key)
    cmd = [
        settings.OPENSSL_PATH,
        'req',       # Certificate request
        '-out',      # output parameter
        csr_file,  # output file name
        '-key',      # private key parameter
        key_file,    # private key file name
        '-new',      # Make new request
        '-subj',     # subject parameter
        subject     # subject of request
    ]

    try:
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=settings.ENV)
        output, error = p.communicate()
        return output
    except Exception as e:
        log('Error occurred creating certificate request: %s' % e.message)
        settings.errorMessage='Error Message: Error occurred creating certificate request: %s' % e.message
    return None

def create_pkcs8(path, name):
    key_file = '%s%s.key' % (path, name)

    cmd = [
        settings.OPENSSL_PATH,
        'pkcs8',
        '-topk8',
        '-in',
        key_file,
        '-inform',
        'pem',
        '-outform',
        'der',
        '-nocrypt'
    ]

    try:
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=settings.ENV)
        output, error = p.communicate()
        if output:
            #save data to file
            file_name = '%s%s.pkcs8' % (path, name)
            file = open(file_name, 'w')
            file.write(output)
            file.close()
            return True
    except Exception as e:
        log('Error occurred creating pkcs8: %s' % e.message)
        settings.errorMessage='Error Message: Error occurred creating pkcs8: %s' % e.message
    return False

def der_to_pem(der, pem, path):
    #log('der to pem')
    der_file = '%s%s' % (path, der)
    pem_file = '%s%s' % (path, pem)
    log('der file: %s' %der_file)
    log('pem file: %s' %pem_file)

    cmd = [
		settings.OPENSSL_PATH,
		'x509',
		'-in',
		der_file,
		'-inform',
		'der',
		'-out',
		pem_file,
		'-outform',
		'pem'
	]

    log('command  %s' %cmd)
    try:
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=settings.ENV)
        output, error = p.communicate()
        return True
    except Exception as e:
        log('Error occurred converting der to pem: %s' % e.message)
        settings.errorMessage='Error Message: Error occurred converting der to pem: %s' % e.message
    return False


def crt_to_der(crt, der, path):
    crt_file = '%s%s' % (path, crt)
    der_file = '%s%s' % (path, der)

    cmd = [
		settings.OPENSSL_PATH,
		'x509',
		'-in',
		crt_file,
		'-out',
		der_file,
		'-outform',
		'der'
	]

    try:
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=settings.ENV)
        output, error = p.communicate()
        return True
    except Exception as e:
        log('Error occurred converting crt to der: %s' % e.message)
        settings.errorMessage='Error Message: Error occurred converting crt to der: %s' % e.message
    return False


def crt_to_pem(crt, pem, path):
    crt_file = '%s%s' % (path, crt)
    der_file = '%s%s' % (path, pem)

    cmd = [
		settings.OPENSSL_PATH,
		'x509',
		'-in',
		crt_file,
		'-out',
		der_file,
		'-outform',
		'pem'
	]

    try:
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=settings.ENV)
        output, error = p.communicate()
        return True
    except Exception as e:
        log('Error occurred converting crt to der: %s' % e.message)
        settings.errorMessage='Error Message: Error occurred converting crt to der: %s' % e.message
    return False