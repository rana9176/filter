This directory contains following CSVs, 
1. Customer_Details.csv
2. Total.csv
3. WindowsDeviceEnrollment.csv

Customer_Details.csv has no. of rows which contains list of provisioned customers with header as 
	customerid,domainid,customername,psn
	This file is used as input to user and group creation script.
    
NOTE : Password - Symc4now!
	   Name of server(domain name)- testmail.qalabs.symantec.com
	   These above two fields are same for all above customers.
	   e.g. Customer token payload- {"email":"${customername}@testmail.qalabs.symantec.com","password":"Symc4now!"}
       
Total.csv has data in format of
	customerid,domainid,custname,groupid,groupname,userid,username,psn,policy_id
	This file used for enrolling devices.
	
WindowsDeviceEnrollment.csv in the format of
	customerid,domainid,custname,userid,deviceid,device_password
	This file used for posting opstate.