package com.symantec.platform.jmeter.gui;

import static com.symantec.platform.jmeter.common.Constants.BLANK;
import static com.symantec.platform.jmeter.common.Constants.EPMP_R3_AMQP;
import static com.symantec.platform.jmeter.common.Constants.MESSAGE_CONTENT;

import com.symantec.platform.jmeter.sampler.EPMPR3AMQPSampler;

import org.apache.jmeter.testelement.TestElement;
import org.apache.jorphan.gui.JLabeledTextArea;

import java.awt.Dimension;

import javax.swing.JPanel;

public class EPMPR3AMQPGui extends AbstractEPMPR3AMQPGui {

	private static final long serialVersionUID = 1L;

	private JPanel publisherPanel;

	private final JLabeledTextArea message = new JLabeledTextArea(MESSAGE_CONTENT);

	public EPMPR3AMQPGui() {
		init();
	}

	@Override
	protected final void init() {
		super.init();
		message.setPreferredSize(new Dimension(400, 150));
		publisherPanel.add(message);
	}

	@Override
	public void configure(TestElement element) {
		super.configure(element);
		if (!(element instanceof EPMPR3AMQPSampler)) {
            return;
        }
		EPMPR3AMQPSampler publisher = (EPMPR3AMQPSampler) element;
		message.setText(publisher.getMessage());
	}

	@Override
	public void modifyTestElement(TestElement element) {
		EPMPR3AMQPSampler puplisher = (EPMPR3AMQPSampler) element;
		puplisher.clear();
		configureTestElement(puplisher);
		super.modifyTestElement(puplisher);
		puplisher.setMessage(message.getText());
	}

	@Override
	public void clearGui() {
		super.clearGui();
		message.setText(BLANK);
	}

	@Override
	public String getStaticLabel() {
		return EPMP_R3_AMQP;
	}


	public String getLabelResource() {
		return this.getClass().getSimpleName();
	}

	public TestElement createTestElement() {
		EPMPR3AMQPSampler publisher = new EPMPR3AMQPSampler();
		modifyTestElement(publisher);
		return publisher;
	}

	@Override
	protected void setMainPanel(JPanel panel) {
		publisherPanel = panel;
	}

}