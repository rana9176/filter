package com.symantec.platform.jmeter.sampler;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Address;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import org.apache.jmeter.samplers.AbstractSampler;
import org.apache.jmeter.testelement.ThreadListener;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public abstract class AbstractEPMPR3AMQPSampler extends AbstractSampler implements ThreadListener {

	private static final long serialVersionUID = 1L;
	
	public static final boolean DEFAULT_EXCHANGE_DURABLE = true;
    public static final boolean DEFAULT_EXCHANGE_AUTODELETE = true;
    public static final boolean DEFAULT_EXCHANGE_REDECLARE = false;
    public static final boolean DEFAULT_QUEUE_REDECLARE = false;

    public static final int DEFAULT_PORT = 5672;
    public static final String DEFAULT_PORT_STRING = Integer.toString(DEFAULT_PORT);

    public static final int DEFAULT_TIMEOUT = 1000;
    public static final String DEFAULT_TIMEOUT_STRING = Integer.toString(DEFAULT_TIMEOUT);

    private static final int DEFAULT_HEARTBEAT = 1;

    private static final Logger log = LoggingManager.getLoggerForClass();

    private static final String EXCHANGE = "R3AMQPSampler.Exchange";
    private static final String EXCHANGE_TYPE = "R3AMQPSampler.ExchangeType";
    private static final String EXCHANGE_DURABLE = "R3AMQPSampler.ExchangeDurable";
    private static final String EXCHANGE_AUTODELETE = "R3AMQPSampler.ExchangeAutoDelete";
    private static final String EXCHANGE_REDECLARE = "R3AMQPSampler.ExchangeRedeclare";
    
    private static final String CUSTOMER_ID = "R3AMQPSampler.CustomerID";
    private static final String DOMAIN_ID = "R3AMQPSampler.DomainID";
    private static final String REQUEST_VERB = "R3AMQPSampler.RequestVerb";
    private static final String REST_URL = "R3AMQPSampler.RestURI";

    private static final String REST_QUERY_STRING = "R3AMQPSampler.RestQueryString";
    private static final String AUTHORIZATION = "R3AMQPSampler.Authorization";
    private static final String UAT = "uat";
    private static final String SESSION_ID = "jmession23456";
    
    private static final String HOST = "R3AMQPSampler.Host";
    private static final String PORT = "R3AMQPSampler.Port";
    private static final String SSL = "R3AMQPSampler.SSL";
    private static final String USERNAME = "R3AMQPSampler.Username";
    private static final String PASSWORD = "R3AMQPSampler.Password";
    private static final String VIRUTAL_HOST = "R3AMQPSampler.VHost";
    private static final String TIMEOUT = "R3AMQPSampler.Timeout";

    private transient ConnectionFactory factory;
    private transient Connection connection;

    protected AbstractEPMPR3AMQPSampler(){
        factory = new ConnectionFactory();
        factory.setRequestedHeartbeat(DEFAULT_HEARTBEAT);
    }

    protected boolean initChannel() throws IOException, NoSuchAlgorithmException, KeyManagementException {
        Channel channel = getChannel();

        if(channel != null && !channel.isOpen()){
            log.warn("channel " + channel.getChannelNumber()
                    + " closed unexpectedly: ", channel.getCloseReason());
            channel = null; // so we re-open it below
        }

        if(channel == null) {
            channel = createChannel();
            setChannel(channel);
        }
        return true;
    }

    protected abstract Channel getChannel();
    protected abstract void setChannel(Channel channel);

    protected BasicProperties getProperties() {
        AMQP.BasicProperties properties = MessageProperties.PERSISTENT_TEXT_PLAIN;
        return properties;
    }

    /**
     * @return a string for the sampleResult Title
     */
    protected String getTitle() {
        return this.getName();
    }

    protected int getTimeoutAsInt() {
        if (getPropertyAsInt(TIMEOUT) < 1) {
            return DEFAULT_TIMEOUT;
        }
        return getPropertyAsInt(TIMEOUT);
    }

    public String getTimeout() {
        return getPropertyAsString(TIMEOUT, DEFAULT_TIMEOUT_STRING);
    }


    public void setTimeout(String s) {
        setProperty(TIMEOUT, s);
    }

    public String getExchange() {
        return getPropertyAsString(EXCHANGE);
    }

    public void setExchange(String name) {
        setProperty(EXCHANGE, name);
    }


    public String getExchangeType() {
        return getPropertyAsString(EXCHANGE_TYPE);
    }

    public void setExchangeType(String name) {
        setProperty(EXCHANGE_TYPE, name);
    }

    public Boolean getExchangeDurable() {
        return getPropertyAsBoolean(EXCHANGE_DURABLE);
    }

    public void setExchangeDurable(Boolean content) {
        setProperty(EXCHANGE_DURABLE, content);
    }

    public Boolean getExchangeAutoDelete() {
        return getPropertyAsBoolean(EXCHANGE_AUTODELETE);
    }

    public void setExchangeAutoDelete(Boolean content) {
        setProperty(EXCHANGE_AUTODELETE, content);
    }
    
    public Boolean getExchangeRedeclare() {
        return getPropertyAsBoolean(EXCHANGE_REDECLARE);
    }

    public void setExchangeRedeclare(Boolean content) {
        setProperty(EXCHANGE_REDECLARE, content);
    }

    /**
     * @return the uat
     */
    public String getUat() {
        return getPropertyAsString(UAT);
    }

    public void setUat(String uat) {
        setProperty(UAT, uat);
    }

    public String getSessionid() {
        return getPropertyAsString(SESSION_ID);
    }

    public void setSessionid(String sessionid) {
        setProperty(SESSION_ID, sessionid);
    }

    public String getCustomerID() {
        return getPropertyAsString(CUSTOMER_ID);
    }

    public void setCustomerID(String customerID) {
        setProperty(CUSTOMER_ID, customerID);
    }

    public String getDomainID() {
        return getPropertyAsString(DOMAIN_ID);
    }

    public void setDomainID(String domainID) {
        setProperty(DOMAIN_ID, domainID);
    }

    public String getRequestVerb() {
        return getPropertyAsString(REQUEST_VERB);
    }

    public void setRequestVerb(String requestVerb) {
        setProperty(REQUEST_VERB, requestVerb);
    }

    public String getRestURI() {
        return getPropertyAsString(REST_URL);
    }

    public void setRestURI(String restURI) {
        setProperty(REST_URL, restURI);
    }

    public String getRestQueryString() {
        return getPropertyAsString(REST_QUERY_STRING);
    }

    public void setRestQueryString(String restQueryString) {
        setProperty(REST_QUERY_STRING, restQueryString);
    }
    
    public String getAuthorization() {
        return getPropertyAsString(AUTHORIZATION);
    }

    public void setAuthorization(String authorization) {
        setProperty(AUTHORIZATION, authorization);
    }    

    public String getVirtualHost() {
        return getPropertyAsString(VIRUTAL_HOST);
    }

    public void setVirtualHost(String name) {
        setProperty(VIRUTAL_HOST, name);
    }

    public String getHost() {
        return getPropertyAsString(HOST);
    }

    public void setHost(String name) {
        setProperty(HOST, name);
    }


    public String getPort() {
        return getPropertyAsString(PORT);
    }

    public void setPort(String name) {
        setProperty(PORT, name);
    }

    protected int getPortAsInt() {
        if (getPropertyAsInt(PORT) < 1) {
            return DEFAULT_PORT;
        }
        return getPropertyAsInt(PORT);
    }

    public void setConnectionSSL(String content) {
        setProperty(SSL, content);
    }

    public void setConnectionSSL(Boolean value) {
        setProperty(SSL, value.toString());
    }

    public boolean connectionSSL() {
        return getPropertyAsBoolean(SSL);
    }


    public String getUsername() {
        return getPropertyAsString(USERNAME);
    }

    public void setUsername(String name) {
        setProperty(USERNAME, name);
    }


    public String getPassword() {
        return getPropertyAsString(PASSWORD);
    }

    public void setPassword(String name) {
        setProperty(PASSWORD, name);
    }

 
    protected void cleanup() {
        try {
            //getChannel().close();   // closing the connection will close the channel if it's still open
            if(connection != null && connection.isOpen()) {
                connection.close();
            }
        } catch (IOException e) {
            log.error("Failed to close connection", e);
        }
    }


    public void threadFinished() {
        log.info("R3AMQPSampler.threadFinished called");
        cleanup();
    }


    public void threadStarted() {

    }

    protected Channel createChannel() throws IOException, NoSuchAlgorithmException, KeyManagementException {
        log.info("Creating channel " + getVirtualHost()+":"+getPortAsInt());

         if (connection == null || !connection.isOpen()) {
            factory.setConnectionTimeout(getTimeoutAsInt());
            factory.setVirtualHost(getVirtualHost());
            factory.setUsername(getUsername());
            factory.setPassword(getPassword());
            factory.useSslProtocol("TLSv1.2");
            /*
             * if (connectionSSL()) {
             * factory.useSslProtocol("TLS");
             * }
             */

            log.info("RabbitMQ ConnectionFactory using:"
                  +"\n\t virtual host: " + getVirtualHost()
                  +"\n\t host: " + getHost()
                  +"\n\t port: " + getPort()
                  +"\n\t username: " + getUsername()
                  +"\n\t password: " + getPassword()
                  +"\n\t timeout: " + getTimeout()
                  +"\n\t heartbeat: " + factory.getRequestedHeartbeat()
                  +"\nin " + this
                  );

            String[] hosts = getHost().split(",");
            Address[] addresses = new Address[hosts.length];
            for (int i = 0; i < hosts.length; i++) {
                addresses[i] = new Address(hosts[i], getPortAsInt());
            }
            log.info("Using hosts: " + Arrays.toString(hosts) + " addresses: " + Arrays.toString(addresses));
            connection = factory.newConnection(addresses);
            log.info("SSL Connection Created");
         }

         Channel channel = connection.createChannel();
        log.info("Channel Created");
         if(!channel.isOpen()){
             log.fatalError("Failed to open channel: " + channel.getCloseReason().getLocalizedMessage());
         }
        return channel;
    }

    protected void deleteQueue() throws IOException, NoSuchAlgorithmException, KeyManagementException {
        // use a different channel since channel closes on exception.
        Channel channel = createChannel();
        try {
        	
        	//To Do
            
        }
        catch(Exception ex) {
            log.debug(ex.toString(), ex);
            // ignore it.
        }
        finally {
            if (channel.isOpen())  {
                channel.close();
            }
        }
    }

    protected void deleteExchange() throws IOException, NoSuchAlgorithmException, KeyManagementException {
        // use a different channel since channel closes on exception.
        Channel channel = createChannel();
        try {
            log.info("Deleting exchange " + getExchange());
            channel.exchangeDelete(getExchange());
        }
        catch(Exception ex) {
            log.debug(ex.toString(), ex);
            // ignore it.
        }
        finally {
            if (channel.isOpen())  {
                channel.close();
            }
        }
    }
}
