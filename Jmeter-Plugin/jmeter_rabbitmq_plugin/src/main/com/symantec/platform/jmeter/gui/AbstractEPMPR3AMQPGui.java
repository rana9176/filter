package com.symantec.platform.jmeter.gui;

import static com.symantec.platform.jmeter.common.Constants.AUTHORIZATION2;
import static com.symantec.platform.jmeter.common.Constants.AUTO_DELETE;
import static com.symantec.platform.jmeter.common.Constants.BLANK;
import static com.symantec.platform.jmeter.common.Constants.CUSTOMER_ID;
import static com.symantec.platform.jmeter.common.Constants.DELETE;
import static com.symantec.platform.jmeter.common.Constants.DIRECT;
import static com.symantec.platform.jmeter.common.Constants.DOMAIN_ID;
import static com.symantec.platform.jmeter.common.Constants.DURABLE;
import static com.symantec.platform.jmeter.common.Constants.EXCHANGE;
import static com.symantec.platform.jmeter.common.Constants.EXCHANGE_TYPE;
import static com.symantec.platform.jmeter.common.Constants.FANOUT;
import static com.symantec.platform.jmeter.common.Constants.GET;
import static com.symantec.platform.jmeter.common.Constants.GUEST;
import static com.symantec.platform.jmeter.common.Constants.HEAD;
import static com.symantec.platform.jmeter.common.Constants.HEADERS;
import static com.symantec.platform.jmeter.common.Constants.HOST;
import static com.symantec.platform.jmeter.common.Constants.LOCALHOST;
import static com.symantec.platform.jmeter.common.Constants.PASSWORD;
import static com.symantec.platform.jmeter.common.Constants.PORT;
import static com.symantec.platform.jmeter.common.Constants.POST;
import static com.symantec.platform.jmeter.common.Constants.REDECLARE;
import static com.symantec.platform.jmeter.common.Constants.REQUEST_VERB;
import static com.symantec.platform.jmeter.common.Constants.REST_QUERY_STRING;
import static com.symantec.platform.jmeter.common.Constants.REST_URI;
import static com.symantec.platform.jmeter.common.Constants.SESSION_ID;
import static com.symantec.platform.jmeter.common.Constants.SLASH;
import static com.symantec.platform.jmeter.common.Constants.TIMEOUT;
import static com.symantec.platform.jmeter.common.Constants.TOPIC;
import static com.symantec.platform.jmeter.common.Constants.UAT;
import static com.symantec.platform.jmeter.common.Constants.USERNAME;
import static com.symantec.platform.jmeter.common.Constants.VIRTUAL_HOST;

import com.symantec.platform.jmeter.sampler.AbstractEPMPR3AMQPSampler;

import org.apache.jmeter.gui.util.VerticalPanel;
import org.apache.jmeter.samplers.gui.AbstractSamplerGui;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jorphan.gui.JLabeledChoice;
import org.apache.jorphan.gui.JLabeledTextField;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

public abstract class AbstractEPMPR3AMQPGui extends AbstractSamplerGui {

	private static final String EPMP_R3_SERVICE_REQUEST = "epmp.r3.service_request";
	private static final long serialVersionUID = 1L;
    private static final Logger log = LoggingManager.getLoggerForClass();

    // Exchanges Declaration
    protected JLabeledTextField exchange = new JLabeledTextField(EXCHANGE);
    protected JLabeledChoice exchangeType = new JLabeledChoice(EXCHANGE_TYPE, new String[]{ DIRECT, TOPIC, HEADERS, FANOUT});
    private final JCheckBox exchangeDurable = new JCheckBox(DURABLE, AbstractEPMPR3AMQPSampler.DEFAULT_EXCHANGE_DURABLE);
    private final JCheckBox exchangeAutoDelete = new JCheckBox(AUTO_DELETE, true);
    private final JCheckBox exchangeRedeclare = new JCheckBox(REDECLARE, AbstractEPMPR3AMQPSampler.DEFAULT_EXCHANGE_REDECLARE);
    
    // Header details required for R3 AMQP Exchange call
    protected JLabeledTextField customerID = new JLabeledTextField(CUSTOMER_ID);
    protected JLabeledTextField domainID = new JLabeledTextField(DOMAIN_ID);
    protected JLabeledChoice requestVerb = new JLabeledChoice(REQUEST_VERB, new String[] {GET, POST, DELETE, HEAD});
    protected JLabeledTextField restURI = new JLabeledTextField(REST_URI);
    protected JLabeledTextField restQueryString = new JLabeledTextField(REST_QUERY_STRING);
    protected JLabeledTextField authorization = new JLabeledTextField(AUTHORIZATION2);
    protected JLabeledTextField uat = new JLabeledTextField(UAT);
    protected JLabeledTextField sessionid = new JLabeledTextField(SESSION_ID);
    

    // RabbitMQ configuration parameters
    protected JLabeledTextField virtualHost = new JLabeledTextField(VIRTUAL_HOST);
    protected JLabeledTextField host = new JLabeledTextField(HOST);
    protected JLabeledTextField port = new JLabeledTextField(PORT);
    protected JLabeledTextField timeout = new JLabeledTextField(TIMEOUT);
    protected JLabeledTextField username = new JLabeledTextField(USERNAME);
    protected JLabeledTextField password = new JLabeledTextField(PASSWORD);
    protected abstract void setMainPanel(JPanel panel);

    @Override
    public void configure(TestElement element) {
    	
        super.configure(element);
        if (!(element instanceof AbstractEPMPR3AMQPSampler)) {
            return;
        }
        AbstractEPMPR3AMQPSampler sampler = (AbstractEPMPR3AMQPSampler) element;

        exchange.setText(sampler.getExchange());
        exchangeType.setText(sampler.getExchangeType());
        exchangeDurable.setSelected(sampler.getExchangeDurable());
        exchangeAutoDelete.setSelected(sampler.getExchangeAutoDelete());
        exchangeRedeclare.setSelected(sampler.getExchangeRedeclare());
        
        customerID.setText(sampler.getCustomerID());
        domainID.setText(sampler.getDomainID());
        requestVerb.setText(sampler.getRequestVerb());
        restURI.setText(sampler.getRestURI());
        restQueryString.setText(sampler.getRestQueryString());
        authorization.setText(sampler.getAuthorization());
        uat.setText(sampler.getUat());
        sessionid.setText(sampler.getSessionid());

        
        virtualHost.setText(sampler.getVirtualHost());
        timeout.setText(sampler.getTimeout());
        host.setText(sampler.getHost());
        port.setText(sampler.getPort());
        username.setText(sampler.getUsername());
        password.setText(sampler.getPassword());
        log.info("AMQPSamplerGui.configure() called");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clearGui() {
    	
        exchange.setText(EPMP_R3_SERVICE_REQUEST);
        exchangeType.setText(DIRECT);
        exchangeDurable.setSelected(AbstractEPMPR3AMQPSampler.DEFAULT_EXCHANGE_DURABLE);
        exchangeAutoDelete.setSelected(AbstractEPMPR3AMQPSampler.DEFAULT_EXCHANGE_AUTODELETE);
        exchangeRedeclare.setSelected(AbstractEPMPR3AMQPSampler.DEFAULT_EXCHANGE_REDECLARE);
        
        customerID.setText(BLANK);
        domainID.setText(BLANK);
        requestVerb.setText(GET);
        restURI.setText(BLANK);
        restQueryString.setText(BLANK);
        authorization.setText(BLANK);
        uat.setText(BLANK);
        sessionid.setText(BLANK);

        virtualHost.setText(SLASH);
        host.setText(LOCALHOST);
        port.setText(AbstractEPMPR3AMQPSampler.DEFAULT_PORT_STRING);
        timeout.setText(AbstractEPMPR3AMQPSampler.DEFAULT_TIMEOUT_STRING);
        username.setText(GUEST);
        password.setText(GUEST);
    }


    public void modifyTestElement(TestElement element) {
        AbstractEPMPR3AMQPSampler sampler = (AbstractEPMPR3AMQPSampler) element;
        sampler.clear();
        configureTestElement(sampler);

        sampler.setExchange(exchange.getText());
        sampler.setExchangeType(exchangeType.getText());
        sampler.setExchangeDurable(exchangeDurable.isSelected());
        sampler.setExchangeAutoDelete(exchangeAutoDelete.isSelected());
        
        sampler.setExchangeRedeclare(exchangeRedeclare.isSelected());
        sampler.setCustomerID(customerID.getText());
        sampler.setDomainID(domainID.getText());
        sampler.setRequestVerb(requestVerb.getText());
        sampler.setRestURI(restURI.getText());
        sampler.setRestQueryString(restQueryString.getText());
        sampler.setAuthorization(authorization.getText());
        sampler.setUat(uat.getText());
        sampler.setSessionid(sessionid.getText());

        
        JMeterUtils.getResString(host.getText());
        sampler.setVirtualHost(virtualHost.getText());
        sampler.setTimeout(timeout.getText());
        sampler.setHost(host.getText());
        sampler.setPort(port.getText());
        sampler.setUsername(username.getText());
        sampler.setPassword(password.getText());
        log.info("AMQPSamplerGui.modifyTestElement() called, set user/pass to " + username.getText() + SLASH + password.getText() + " on sampler " + sampler);
    }

    protected void init() {
        setLayout(new BorderLayout(0, 5));
        setBorder(makeBorder());
        add(makeTitlePanel(), BorderLayout.NORTH); // Add the standard title
        
        JPanel mainPanel = new VerticalPanel();
        mainPanel.add(makeCommonPanel());
        add(mainPanel);
        
        setMainPanel(mainPanel);
    }

    private Component makeCommonPanel() {
        GridBagConstraints gridBagConstraints, gridBagConstraintsCommon;

        gridBagConstraintsCommon = new GridBagConstraints();
        gridBagConstraintsCommon.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraintsCommon.anchor = GridBagConstraints.WEST;
        gridBagConstraintsCommon.weightx = 0.5;

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        gridBagConstraints.fill = GridBagConstraints.NONE;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.5;
        JPanel exchangeSettings = new JPanel(new GridBagLayout());
        exchangeGridDeclaration(gridBagConstraints, exchangeSettings);

        JPanel queueSettings = new JPanel(new GridBagLayout());
        r3ParametersGridDeclartion(gridBagConstraints, gridBagConstraintsCommon, queueSettings);

        JPanel exchangeQueueSettings = new VerticalPanel();
        JPanel commonPanel = new JPanel(new GridBagLayout());
        exchangeQueueSettings.add(exchangeSettings);
        exchangeQueueSettings.add(queueSettings);
        commonPanel.add(exchangeQueueSettings, gridBagConstraintsCommon);

        JPanel serverSettings = new JPanel(new GridBagLayout());
        rabbitMQConfigGridDeclartion(gridBagConstraints, gridBagConstraintsCommon, serverSettings);
        commonPanel.add(serverSettings, gridBagConstraintsCommon);

        return commonPanel;
    }

	private void rabbitMQConfigGridDeclartion(GridBagConstraints gridBagConstraints, GridBagConstraints gridBagConstraintsCommon, JPanel serverSettings) {
		serverSettings.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "RabbitMQ Connection"));

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        serverSettings.add(virtualHost, gridBagConstraints);

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        serverSettings.add(host, gridBagConstraints);

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        serverSettings.add(port, gridBagConstraints);

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        serverSettings.add(username, gridBagConstraints);

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        serverSettings.add(password, gridBagConstraints);

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        serverSettings.add(timeout, gridBagConstraints);

              gridBagConstraintsCommon.gridx = 1;
        gridBagConstraintsCommon.gridy = 0;
	}

	private void r3ParametersGridDeclartion(GridBagConstraints gridBagConstraints, GridBagConstraints gridBagConstraintsCommon, JPanel queueSettings) {
		queueSettings.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "R3 AMQP Headers"));

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        queueSettings.add(requestVerb, gridBagConstraints);

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        queueSettings.add(customerID, gridBagConstraints);

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        queueSettings.add(domainID, gridBagConstraints);

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        queueSettings.add(restURI, gridBagConstraints);

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        queueSettings.add(restQueryString, gridBagConstraints);


        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        queueSettings.add(authorization, gridBagConstraints);

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        queueSettings.add(uat, gridBagConstraints);

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        queueSettings.add(sessionid, gridBagConstraints);

        gridBagConstraintsCommon.gridx = 0;
        gridBagConstraintsCommon.gridy = 0;
	}

	private void exchangeGridDeclaration(GridBagConstraints gridBagConstraints, JPanel exchangeSettings) {
		exchangeSettings.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), EXCHANGE));
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        exchangeSettings.add(exchange, gridBagConstraints);

        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        exchangeSettings.add(exchangeType, gridBagConstraints);

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        exchangeSettings.add(exchangeDurable, gridBagConstraints);

        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        exchangeSettings.add(exchangeAutoDelete, gridBagConstraints);
        
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        exchangeSettings.add(exchangeRedeclare, gridBagConstraints);
	}

}
