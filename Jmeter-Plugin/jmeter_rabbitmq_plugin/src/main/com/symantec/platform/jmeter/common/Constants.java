package com.symantec.platform.jmeter.common;

public class Constants {

	private Constants() {
		throw new AssertionError();
	}

	public static final String BLANK = "";

	public static final String SLASH = "/";
	public static final String LOCALHOST = "localhost";
	public static final String PASSWORD = "Password";
	public static final String USERNAME = "Username";
	public static final String GUEST = "guest";
	public static final String TIMEOUT = "Timeout";
	public static final String PORT = "Port";
	public static final String HOST = "Host";
	public static final String VIRTUAL_HOST = "Virtual Host";

	public static final String AUTHORIZATION2 = "Authorization";
	public static final String REST_QUERY_STRING = "Rest Query String";
	public static final String REST_URI = "Rest URI";
	public static final String REQUEST_VERB = "Request Verb";
	public static final String GET = "GET";
	public static final String POST = "POST";
	public static final String HEAD = "HEAD";
	public static final String DELETE = "DELETE";
	public static final String DOMAIN_ID = "Domain ID";
	public static final String CUSTOMER_ID = "Customer ID";
    public static final String UAT = "UAT Header";
    public static final String SESSION_ID = "sessionid";


	public static final String REDECLARE = "Redeclare?";
	public static final String AUTO_DELETE = "Auto Delete?";
	public static final String DURABLE = "Durable?";
	public static final String FANOUT = "fanout";
	public static final String HEADERS = "headers";
	public static final String TOPIC = "topic";
	public static final String DIRECT = "direct";
	public static final String EXCHANGE_TYPE = "Exchange Type";
	public static final String EXCHANGE = "Exchange";
	public static final String EPMP_R3_AMQP = "EPMP R3 AMQP";

	public static final String MESSAGE_CONTENT = "Message Content";
}
