package com.symantec.platform.jmeter.sampler;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;

import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.samplers.Interruptible;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.testelement.property.CollectionProperty;
import org.apache.jmeter.testelement.property.JMeterProperty;
import org.apache.jmeter.testelement.property.PropertyIterator;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

/**
 * JMeter creates an instance of a sampler class for every occurrence of the
 * element in every thread. [some additional copies may be created before the
 * test run starts]
 *
 * Thus each sampler is guaranteed to be called by a single thread - there is no
 * need to synchronize access to instance variables.
 *
 * However, access to class fields must be synchronized.
 */
public class EPMPR3AMQPSampler extends AbstractEPMPR3AMQPSampler implements Interruptible {

	private static final long serialVersionUID = 1l;
	private static final Logger log = LoggingManager.getLoggerForClass();

	private final static String MESSAGE = "AMQPPublisher.Message";
	private final static String MESSAGE_ROUTING_KEY = "AMQPPublisher.MessageRoutingKey";
	private final static String MESSAGE_TYPE = "AMQPPublisher.MessageType";
	private final static String REPLY_TO_QUEUE = "AMQPPublisher.ReplyToQueue";
	private final static String CORRELATION_ID = "AMQPPublisher.CorrelationId";
	private final static String PERSISTENT = "AMQPConsumer.Persistent";
	private final static String USE_TX = "AMQPConsumer.UseTx";
	
	public static boolean DEFAULT_PERSISTENT = false;
	public static boolean DEFAULT_USE_TX = false;
	public static final String BOOTSTRAP_URI = "/v1/mdr/bootstrap/user";
	
	private transient Channel channel;

	public EPMPR3AMQPSampler() {
		super();
	}


	public SampleResult sample(Entry e) {
	    String response = null;
	    String status = null;
	    
		SampleResult result = new SampleResult();
		result.setSampleLabel(getName());
		
		// Default Result Set
		if (!initChannel(result)) {
			return result;
		}

		String data = getMessage(); // Sampler data

		try {
			
			String replyQueueName = channel.queueDeclare().getQueue();
			QueueingConsumer consumer = new QueueingConsumer(channel);
		    channel.basicConsume(replyQueueName, true, consumer);
            // channel.queueBind(paramString1, paramString2, paramString3)
		    
			int deliveryMode = getPersistent() ? 2 : 1;
			
			String corrId = UUID.randomUUID().toString();
			
			Map<String, Object> headers = getHeaders();
			headers.put("Accept", "application/json");
			headers.put("x-epmp-customer-id", getCustomerID());
			headers.put("x-epmp-domain-id", getDomainID());
			headers.put("x-rest-verb", getRequestVerb());
			headers.put("x-rest-uri", getRestURI());
			headers.put("x-rest-querystring", getRestQueryString());
			headers.put("x-rest-accept", "application/json");
			headers.put("Authorization", getAuthorization());
            headers.put("x-epmp-uat", getUat());
            headers.put("x-epmp-session-id", getSessionid());
            headers.put("x-epmp-timezone", TimeZone.getTimeZone("America/Lima").getID());
            headers.put("x-epmp-locale", Locale.US.toLanguageTag());
			
            log.info(headers.toString());
            
			AMQP.BasicProperties publishProperties = new AMQP.BasicProperties.Builder()
.contentType("application/json;charset=UTF-8")
                    .contentEncoding(StandardCharsets.UTF_8.name())
				.timestamp(new Date())
				.headers(headers)
				.deliveryMode(deliveryMode)
				.correlationId(corrId).replyTo(replyQueueName)
				.messageId(corrId)
				.build();	
			
            log.info("Publish Properties: " + publishProperties.toString());

			byte[] messageBytes = getMessageBytes();
			result.sampleStart(); // Start timing
            log.info("for debugging purpose");
            log.info(getExchange());
            try {
			channel.basicPublish(getExchange(), "",	publishProperties, messageBytes);
            } catch (Exception ex) {
                log.info(ex.getMessage(), ex);
            }

		    while (true) {
                log.info("Inside Response");
		        QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                log.info(corrId);
                log.info("Next line is to print from delivery");
                log.info(delivery.getProperties().getCorrelationId());
                log.info("Next line is to print the status");
                log.info(delivery.getProperties().getHeaders().toString());
                //log.info(delivery.getProperties().getHeaders().get("status").toString());
                log.info(delivery.getProperties().getHeaders().get("x-rest-status").toString());

		        if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                    log.info("inside corelation check");
                    // response = new String(delivery.getBody(),"UTF-8");
                    // log.info(delivery.toString());
                    // log.info(response);
                    status = delivery.getProperties().getHeaders().get("x-rest-status").toString();
                    /*
                     * if (BOOTSTRAP_URI.equals(getRestURI())){
                     * status =
                     * delivery.getProperties().getHeaders().get("x-rest-status").toString();
                     * }
                     * else{
                     * status = delivery.getProperties().getHeaders().get("status").toString();
                     * log.info("Status: " + status);
                     * }
                     */
		          break;
		        }
		      }
		    
			// commit the sample.
			if (getUseTx()) {
				channel.txCommit();
			}
			
			// Set up the sample result details
            setResult(result, data, status.getBytes(StandardCharsets.UTF_8.name()), status);
			
		} catch (Exception ex) {
			log.debug(ex.getMessage(), ex);
			markFailure(result, ex);
		} finally {
			result.sampleEnd(); // End timimg
		}

		return result;
	}

	private void markFailure(SampleResult result, Exception ex) {
		result.setResponseCode("400");
		result.setSuccessful(false);
		result.setResponseMessage(ex.toString());
	}

	private void setResult(SampleResult result, String data, byte[] messageBytes, String status) {
		result.setSamplerData(data);
        result.setResponseData(new String(messageBytes), null);
		result.setResponseCode(status);
		result.setResponseMessage("OK");
		result.setDataType(SampleResult.TEXT);
		result.setSuccessful(true);
	}

	private Map<String, Object> getHeaders() {
		CollectionProperty collectionProperties = ((CollectionProperty) getProperty("Arguments.arguments"));
		Map<String, Object> header = new HashMap<String, Object>();
		if (collectionProperties != null && collectionProperties.size() != 0) {
			PropertyIterator propIterator = collectionProperties.iterator();
			while (propIterator.hasNext()) {
				JMeterProperty prop = propIterator.next();
				if (prop.getName().startsWith("x-")) {
					header.put(
							prop.getName(),
							prop.getStringValue().substring(
									prop.getStringValue().indexOf("=") + 1));
				}

			}
		}
		return header;
	}

	private byte[] getMessageBytes() {
		return getMessage().getBytes();
	}

	public String getMessageRoutingKey() {
		return getPropertyAsString(MESSAGE_ROUTING_KEY);
	}

	public void setMessageRoutingKey(String content) {
		setProperty(MESSAGE_ROUTING_KEY, content);
	}

	public String getMessage() {
		return getPropertyAsString(MESSAGE);
	}

	public void setMessage(String content) {
		setProperty(MESSAGE, content);
	}

	public String getMessageType() {
		return getPropertyAsString(MESSAGE_TYPE);
	}

	public void setMessageType(String content) {
		setProperty(MESSAGE_TYPE, content);
	}

	public String getReplyToQueue() {
		return getPropertyAsString(REPLY_TO_QUEUE);
	}

	public void setReplyToQueue(String content) {
		setProperty(REPLY_TO_QUEUE, content);
	}

	public String getCorrelationId() {
		return getPropertyAsString(CORRELATION_ID);
	}

	public void setCorrelationId(String content) {
		setProperty(CORRELATION_ID, content);
	}

	public Boolean getPersistent() {
		return getPropertyAsBoolean(PERSISTENT, DEFAULT_PERSISTENT);
	}

	public void setPersistent(Boolean persistent) {
		setProperty(PERSISTENT, persistent);
	}

	public Boolean getUseTx() {
		return getPropertyAsBoolean(USE_TX, DEFAULT_USE_TX);
	}

	public void setUseTx(Boolean tx) {
		setProperty(USE_TX, tx);
	}


	public boolean interrupt() {
		cleanup();
		return true;
	}

	@Override
	protected Channel getChannel() {
		return channel;
	}

	@Override
	protected void setChannel(Channel channel) {
		this.channel = channel;
	}

	@Override
	protected AMQP.BasicProperties getProperties() {
		AMQP.BasicProperties parentProps = super.getProperties();

		int deliveryMode = getPersistent() ? 2 : 1;

		AMQP.BasicProperties publishProperties = new AMQP.BasicProperties.Builder()
			.contentType(getMessageType())
				.contentEncoding(parentProps.getContentEncoding())
				.headers(getHeaders()).deliveryMode(deliveryMode)
				.priority(parentProps.getPriority())
				.correlationId(getCorrelationId()).replyTo(getReplyToQueue())
				.messageId(parentProps.getMessageId())
				.timestamp(parentProps.getTimestamp())
				.userId(parentProps.getUserId()).appId(parentProps.getAppId())
				.clusterId(parentProps.getClusterId())
				.expiration(parentProps.getExpiration()).build();	


		return publishProperties;
	}

	protected boolean initChannel(SampleResult result) {

		boolean ret = false;
		try {
			ret = super.initChannel();
			if (getUseTx()) {
				channel.txSelect();
			}
		} catch (Exception ex) {
			log.error("Failed to initialize channel : ", ex);
			result.setResponseMessage(ex.toString());
			return false;
		}
		return ret;
	}
}
