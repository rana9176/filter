# How To Build this Project #
============================

git clone https://<user_name>@bitbucket-sed.broadcom.net/scm/suepbeachhead/psr.git
cd psr/Jmeter-Plugin/jmeter_rabbitmq_plugin
ant clean package


At minimum, to create a custom rabbitmq sampler, we need to have a class extend AbstractSampler and provide an implementation for the sample (entry) method. 

References - https://github.com/jlavallee/JMeter-Rabbit-AMQP


# JMeter-Rabbit-AMQP #
======================

A [JMeter](http://jmeter.apache.org/) plugin to publish & consume messages from [RabbitMQ]


JMeter Runtime Dependencies
---------------------------

Prior to building or installing this JMeter plugin, ensure that the RabbitMQ client library (amqp-client-3.x.x.jar) is installed in JMeter's lib/ directory.


Build Dependencies
------------------

Build dependencies are managed by Ivy. JARs should automagically be downloaded by Ivy as part of the build process.

In addition, you'll need to copy or symlink the following from JMeter's lib/ext directory:
* ApacheJMeter_core.jar


Building
--------

The project is built using Ant. To execute the build script, just execute:
    ant


Installing
----------

To install the plugin, build the project and copy the generated JMeterAMQP.jar file from target/dist to JMeter's lib/ext/ directory.
