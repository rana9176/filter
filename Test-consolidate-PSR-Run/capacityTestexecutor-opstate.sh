#!/bin/sh

JMETER_PATH=/opt/apache-jmeter-2.13/bin
JMETER_LIB=/opt/apache-jmeter-2.13/lib

JMX_PATH=/opt/opstatetest
BASE_PATH=/opt/opstatetest
RESULT_BASE_PATH=/opt/opstatetest/results

THREADS=$1
TIME=$2

	Test_NAME="Opstate-"$RANDOM
	mkdir -p ${RESULT_BASE_PATH}/${Test_NAME}
        test_result_folder=${RESULT_BASE_PATH}/${Test_NAME}
        cp -r /opt/csv-to-html-table-master/csv-to-html-table-master/* ${test_result_folder}/
	cd ${JMETER_PATH}
	#startTime=$(date -d '+5 hour 30 min' -u +"%Y-%m-%dT%H:%M:%S.%3NZ")
	startTime=$(date)
        echo "Start Time : ${startTime}" >> ${test_result_folder}/testduration.txt

	sh jmeter.sh -n -t ${JMX_PATH}/PostOpstate.jmx -l ${test_result_folder}/${Test_NAME}.jtl -Joutput_folder=${BASE_PATH}/ -Jconcurrentthreads=${THREADS} -Jduration=${TIME} -Jtokenserver=100.122.195.121 -Jresulttemp=${test_result_folder}/ -JvalidationWait=3600000 > ${test_result_folder}/jmeterconsoleoutput.txt

        java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-csv "${test_result_folder}/data/agreegatereport.csv" --input-jtl "${test_result_folder}/${Test_NAME}.jtl" --plugin-type AggregateReport	
        java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-png "${test_result_folder}/images/RTOT.png" --input-jtl "${test_result_folder}/${Test_NAME}.jtl" --plugin-type ResponseTimesOverTime --width 800 --height 600

        java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-png "${test_result_folder}/images/TPS.png" --input-jtl "${test_result_folder}/${Test_NAME}.jtl" --plugin-type TransactionsPerSecond --width 800 --height 600
        java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-png "${test_result_folder}/images/HPS.png" --input-jtl "${test_result_folder}/${Test_NAME}.jtl" --plugin-type HitsPerSecond --width 800 --height 600


	mv ${JMETER_PATH}/jmeter.log ${test_result_folder}
	
	endTime=$(date)
	mv ${JMETER_PATH}/jmeter.log ${test_result_folder}
	mv ${BASE_PATH}/*.csv ${test_result_folder}/
	mv ${test_result_folder}/WindowsDeviceEnrollment.csv ${BASE_PATH}/
        echo "End Time : ${endTime}" >> ${test_result_folder}/testduration.txt
