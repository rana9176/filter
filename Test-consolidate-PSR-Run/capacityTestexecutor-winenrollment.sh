#!/bin/sh

JMETER_PATH=/opt/apache-jmeter-2.13/bin
JMETER_LIB=/opt/apache-jmeter-2.13/lib

JMX_PATH=/opt/Data-Population
BASE_PATH=/opt/Data-Population
RESULT_BASE_PATH=/opt/Data-Population/results

THREADS=$1

	Test_NAME="Windows-Enrollment-"$RANDOM
	mkdir -p ${RESULT_BASE_PATH}/${Test_NAME}
        test_result_folder=${RESULT_BASE_PATH}/${Test_NAME}
        cp -r /opt/csv-to-html-table-master/csv-to-html-table-master/* ${test_result_folder}/
	cd ${JMETER_PATH}
	startTime=$(date)
        echo "Start Time : ${startTime}" >> ${test_result_folder}/testduration.txt

        sh jmeter.sh -n -t ${JMX_PATH}/WindowsEnrollment.jmx -l ${test_result_folder}/${Test_NAME}.jtl -Joutput_folder=${BASE_PATH}/Data/ -Jthreads=${THREADS} -Jtokenserver=100.122.195.121 > ${test_result_folder}/jmeterconsoleoutput.txt

        java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-csv "${test_result_folder}/data/agreegatereport.csv" --input-jtl "${test_result_folder}/${Test_NAME}.jtl" --plugin-type AggregateReport	
        java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-png "${test_result_folder}/images/RTOT.png" --input-jtl "${test_result_folder}/${Test_NAME}.jtl" --plugin-type ResponseTimesOverTime --width 800 --height 600

        java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-png "${test_result_folder}/images/TPS.png" --input-jtl "${test_result_folder}/${Test_NAME}.jtl" --plugin-type TransactionsPerSecond --width 800 --height 600
        java -jar $JMETER_LIB/ext/CMDRunner.jar --tool Reporter --generate-png "${test_result_folder}/images/HPS.png" --input-jtl "${test_result_folder}/${Test_NAME}.jtl" --plugin-type HitsPerSecond --width 800 --height 600

	mv ${JMETER_PATH}/jmeter.log ${test_result_folder}
	endTime=$(date)

        echo "End Time : ${endTime}" >> ${test_result_folder}/testduration.txt
